package com.vndr.smartrenja.domain.source.local.dao

import androidx.room.*
import com.vndr.smartrenja.domain.source.model.User
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Flowable
import io.reactivex.rxjava3.core.Single

@Dao
abstract class UserDao {

    @Transaction
    open fun clearAndInsertAccount(user: User) {
        clearAll()
        insert(user)
    }

    @Query("DELETE FROM user_table")
    abstract fun clearAll(): Completable

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insert(user: User): Long

    @Query("SELECT * FROM user_table LIMIT 1")
    abstract fun getAccountFlowable(): Flowable<List<User>>

    @Query("SELECT * FROM user_table LIMIT 1")
    abstract fun getAccount(): Single<User>

    @Query("SELECT COUNT(*) FROM user_table")
    abstract fun getUserCount(): Single<Int>
}