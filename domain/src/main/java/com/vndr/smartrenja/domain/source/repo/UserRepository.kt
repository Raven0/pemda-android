package com.vndr.smartrenja.domain.source.repo

import com.vndr.smartrenja.domain.base.BaseRepository
import com.vndr.smartrenja.domain.source.local.dao.TokenDao
import com.vndr.smartrenja.domain.source.local.dao.UserDao
import com.vndr.smartrenja.domain.source.model.User
import com.vndr.smartrenja.domain.source.model.state.Resource
import com.vndr.smartrenja.domain.source.remote.ApiService
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Flowable
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.schedulers.Schedulers
import javax.inject.Inject

class UserRepository @Inject constructor(
    private val service: ApiService,
    private val userDao: UserDao,
    private val tokenDao: TokenDao,
) : BaseRepository() {
    fun getUser(): Single<Resource<User>> =
        userDao.getAccount().prepareLocalRequest().addSchedulers()

    fun getUserLocal(): Flowable<Resource<User>> =
        userDao.getAccountFlowable().addSchedulers().onBackpressureLatest()
            .prepareLocalFlowableRequest()

    fun fetchUser(): Single<Resource<User>> =
        service.getProfile().prepareNetworkRequest()
            .map {
                if (it is Resource.Success) it.data?.let { user ->
                    userDao.insert(user)
                }
                return@map it
            }
            .addSchedulers()

    fun logout(): Completable = service
        .logout()
        .prepareNetworkRequest()
        .flatMapCompletable {
            Completable.mergeArray(
                userDao.clearAll(),
                tokenDao.delete()
            )
        }
        .addSchedulers()
}