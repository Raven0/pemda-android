package com.vndr.smartrenja.domain.model

import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName
import java.io.Serializable

@Keep
data class ProgramKerja(
    @SerializedName("id") val id: Int?,
    @SerializedName("program_kerja_id") val programKerjaId: Int?,
    @SerializedName("nama_program") val namaProgram: String?,
    @SerializedName("total_rencana_kerja") val totalRencanaKerja: Int?,
    @SerializedName("total_rapat") val totalRapat: Int?,
    @SerializedName("total_kunker") val totalKunker: Int?
) : Serializable