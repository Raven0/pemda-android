package com.vndr.smartrenja.domain.model

import androidx.annotation.Keep
import androidx.room.*
import com.google.gson.annotations.SerializedName
import java.io.Serializable

@Keep
data class Banner(
    @PrimaryKey
    @SerializedName("id") val id: Long?,
    @SerializedName("gambar") var gambar: String?,
    @SerializedName("created_at") var createdAt: String?,
    @SerializedName("updated_at") var updatedAt: String?
) : Serializable