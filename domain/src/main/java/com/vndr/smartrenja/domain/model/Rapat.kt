package com.vndr.smartrenja.domain.model

import androidx.annotation.Keep
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import java.io.Serializable
import java.time.LocalDate

@Keep
data class Rapat(
    @PrimaryKey
    @SerializedName("id") val id: Int?,
    @SerializedName("rencana_kerja_id") val rencanaKerjaId: String?,
    @SerializedName("rencana_kerja") val rencanaKerja: String?,
    @SerializedName("program_kerja_id") val programKerjaId: Int?,
    @SerializedName("program_kerja") val programKerja: String?,
    @SerializedName("dasar_rapat") val dasarRapat: String?,
    @SerializedName("sub_tema_rapat") val subTemaRapat: String?,
    @SerializedName("tema_rapat") val temaRapat: String?,
    @SerializedName("nomor") val nomor: String?,
    @SerializedName("jenis") val jenis: String?,
    @SerializedName("pimpinan") val pimpinan: String?,
    @SerializedName("sekretariat") val sekretariat: String?,
    @SerializedName("tanggal") val tanggal: String?,
    @SerializedName("notulen") val notulen: String?,
    @SerializedName("created_at") val createdAt: String?,
    @SerializedName("updated_at") val updatedAt: String?,
    @SerializedName("is_approve_hasil_rapat") val isApproveHasilRapat: Boolean?,
    @SerializedName("is_approve_materi_rapat") val isApproveMateriRapat: Boolean?,
    @SerializedName("status_hasil_rapat") val statusHasilRapat: String?,
    @SerializedName("status_materi_rapat") val statusMateriRapat: String?,
    @SerializedName("catatan_perbaikan_hasil_rapat") val catatanHasilRapat: String?,
    @SerializedName("catatan_perbaikan_materi_rapat") val catatanMateriRapat: String?,
    @SerializedName("peserta") val peserta: List<Peserta>?,
    @SerializedName("galleries") val galleries: List<Gallery>?,
    @SerializedName("files") val files: Files?,
    val date: LocalDate
) : Serializable

@Keep
data class Peserta(
    @PrimaryKey
    @SerializedName("id") val id: Int?,
    @SerializedName("nama_anggota") val namaAnggota: String?,
    @SerializedName("is_external") val isExternal: Boolean?,
    @SerializedName("photo") val photo: String?
) : Serializable

@Keep
data class Gallery(
    @SerializedName("photo_original") val photoOri: String?,
    @SerializedName("photo_thumbnail") val photoThumbnail: String?,
    @SerializedName("desc") val desc: String?
) : Serializable

@Keep
data class Files(
    @SerializedName("hasil_rapat") val hasil_rapat: String?,
    @SerializedName("file_materi") val file_materi: String?,
    @SerializedName("file_materi_global") val file_materi_global: String?,
    @SerializedName("file_laporan_kunker") val file_laporan_kunker: String?,
    @SerializedName("file_materi_reses") val file_materi_reses: String?,
    @SerializedName("file_laporan_reses") val file_laporan_reses: String?
) : Serializable