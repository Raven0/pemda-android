package com.vndr.smartrenja.domain.source.repo

import com.google.gson.Gson
import com.vndr.smartrenja.domain.base.BaseRepository
import com.vndr.smartrenja.domain.model.FirebaseToken
import com.vndr.smartrenja.domain.source.local.PrefManager
import com.vndr.smartrenja.domain.source.local.dao.TokenDao
import com.vndr.smartrenja.domain.source.local.dao.UserDao
import com.vndr.smartrenja.domain.source.model.Token
import com.vndr.smartrenja.domain.source.model.request.LoginRequest
import com.vndr.smartrenja.domain.source.model.state.Resource
import com.vndr.smartrenja.domain.source.remote.ApiService
import io.reactivex.rxjava3.core.Single
import java.util.*
import javax.inject.Inject

class AuthRepository @Inject constructor(
    private val gson: Gson,
    private val apiService: ApiService,
    private val prefManager: PrefManager,
    private val userDao: UserDao,
    private val tokenDao: TokenDao
) : BaseRepository() {
    fun login(username: String, password: String, rememberMe: Boolean) = apiService.login(
        LoginRequest(username, password)
    ).map { res ->
        return@map Single.just(
            if (res.data != null) {
                res.data.let { user ->
                    tokenDao.deleteAndInsertAccessToken(Token(user.token!!))
                }
                Resource.Success(res.data)
            } else Resource.Error.ApiError(res.message, res.code)
        ).blockingGet()
    }.flatMap {
        return@flatMap apiService.getProfile()
    }.prepareNetworkRequest().map {
        if (it is Resource.Success) it.data?.let { user ->
            userDao.insert(user)
            prefManager.setLogin(rememberMe)

            val akd = user.akd?.get(0)
            prefManager.setMenuAkd(gson.toJson(akd))
        }
        return@map it
    }.addSchedulers()

    fun saveToken(): Single<Resource<FirebaseToken>> =
        Single.just(prefManager.getFirebaseToken().orEmpty())
            .flatMap {
                apiService.setFirebaseToken(it).prepareNetworkRequest().addSchedulers()
            }
}

