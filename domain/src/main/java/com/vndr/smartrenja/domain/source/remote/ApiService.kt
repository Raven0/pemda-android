package com.vndr.smartrenja.domain.source.remote

import com.vndr.smartrenja.domain.model.*
import com.vndr.smartrenja.domain.model.response.StandardResponse
import com.vndr.smartrenja.domain.source.model.User
import com.vndr.smartrenja.domain.source.model.request.LoginRequest
import io.reactivex.rxjava3.annotations.Nullable
import io.reactivex.rxjava3.core.Single
import retrofit2.Response
import retrofit2.http.*

interface ApiService {
    @POST("auth/login")
    fun login(@Body loginRequest: LoginRequest): Single<StandardResponse<UserAccessToken>>

    @POST("auth/logout")
    fun logout(): Single<StandardResponse<Nullable>>

    @FormUrlEncoded
    @POST("profile/set-token")
    fun setFirebaseToken(@Field("fcm_token") token: String): Single<StandardResponse<FirebaseToken>>

    @GET("notification/get")
    fun getNotifications(): Single<StandardResponse<List<Notification>>>

    @POST("notification/clear")
    fun clearNotifications(): Single<StandardResponse<Nothing>>

    @POST("notification/mark-as-read-with-id")
    fun readNotifications(@Query("notif_id") notifId: String): Single<StandardResponse<Nothing>>

    @POST("profile/details")
    fun getProfile(): Single<StandardResponse<User>>

    @GET("banners")
    fun getBanner(): Single<StandardResponse<List<Banner>>>

    @GET("akd/anggota-by-akd")
    fun getAnggotaByAkd(@Query("akd_id") akdId: Int): Single<StandardResponse<List<Anggota>>>

    @GET("rencana-kerja/by-proker")
    fun getRenjaByProker(
        @Query("program_kerja_id") programKerjaId: Int,
        @Query("master_akd_id") masterAkdId: Int,
        @Query("tahun") tahun: Int
    ): Single<StandardResponse<List<Renja>>>

    @POST("laporan-progja/approve/laporan-progja")
    fun approveLaporanProgja(
        @Query("rencana_kerja_id") renjaId: Int
    ): Single<StandardResponse<Nothing>>

    @POST("laporan-progja/perbaikan/laporan-progja")
    fun perbaikanLaporanProgja(
        @Query("rencana_kerja_id") renjaId: Int,
        @Query("message") message: String
    ): Single<StandardResponse<Nothing>>

    @GET("rapat/by-renja")
    fun getRapat(
        @Query("rencana_kerja_id") renjaId: Int
    ): Single<StandardResponse<List<Rapat>>>

    @GET("rapat/detail")
    fun getRapatDetail(
        @Query("rapat_id") rapatId: Int
    ): Single<StandardResponse<Rapat>>

    @POST("rapat/approve/materi-rapat")
    fun approveMateriRapat(
        @Query("rapat_id") rapatId: Int
    ): Single<StandardResponse<Nothing>>

    @POST("rapat/approve/hasil-rapat")
    fun approveHasilRapat(
        @Query("rapat_id") rapatId: Int
    ): Single<StandardResponse<Nothing>>

    @POST("rapat/perbaikan/materi-rapat")
    fun perbaikanMateriRapat(
        @Query("rapat_id") rapatId: Int,
        @Query("message") message: String
    ): Single<StandardResponse<Nothing>>

    @POST("rapat/perbaikan/hasil-rapat")
    fun perbaikanHasilRapat(
        @Query("rapat_id") rapatId: Int,
        @Query("message") message: String
    ): Single<StandardResponse<Nothing>>

    @GET("kunker/by-renja")
    fun getKunker(
        @Query("rencana_kerja_id") renjaId: Int
    ): Single<StandardResponse<List<Kunker>>>

    @GET("kunker/detail")
    fun getKunkerDetail(
        @Query("kunker_id") kunkerId: Int
    ): Single<StandardResponse<Kunker>>

    @POST("kunker/approve/materi-kunker")
    fun approveMateriKunker(
        @Query("kunker_id") kunkerId: Int
    ): Single<StandardResponse<Nothing>>

    @POST("kunker/approve/laporan-kunker")
    fun approveLaporanKunker(
        @Query("kunker_id") kunkerId: Int
    ): Single<StandardResponse<Nothing>>

    @POST("kunker/perbaikan/materi-kunker")
    fun perbaikanMateriKunker(
        @Query("kunker_id") kunkerId: Int,
        @Query("message") message: String
    ): Single<StandardResponse<Nothing>>

    @POST("kunker/perbaikan/laporan-kunker")
    fun perbaikanLaporanKunker(
        @Query("kunker_id") kunkerId: Int,
        @Query("message") message: String
    ): Single<StandardResponse<Nothing>>

    @GET("schedule/user-schedules")
    fun getSchedule(
        @Query("program_kerja_id") programKerjaId: Int?,
        @Query("master_akd_id") masterAkdId: Int?,
        @Query("tahun") tahun: Int?
    ): Single<StandardResponse<List<Schedule>>>

    @GET("schedule/upcoming-activities")
    fun getUpcomingSchedule(): Single<StandardResponse<List<Schedule>>>

    @GET("faq")
    fun getFaq(): Single<StandardResponse<List<Faqs>>>

    @GET("kebijakan-privasi")
    fun getTerms(): Single<StandardResponse<List<Faqs>>>

    @GET("reses/by-user")
    fun getReses(
        @Query("tahun") tahun: Int?
    ): Single<StandardResponse<List<Reses>>>

    @GET("reses/detail")
    fun getResesDetail(
        @Query("reses_id") rapatId: Int
    ): Single<StandardResponse<Reses>>


    @POST("reses/approve/materi-reses")
    fun approveMateriReses(
        @Query("reses_id") rapatId: Int
    ): Single<StandardResponse<Nothing>>

    @POST("reses/approve/laporan-reses")
    fun approveHasilReses(
        @Query("reses_id") rapatId: Int
    ): Single<StandardResponse<Nothing>>

    @POST("reses/perbaikan/materi-reses")
    fun perbaikanMateriReses(
        @Query("reses_id") rapatId: Int,
        @Query("message") message: String
    ): Single<StandardResponse<Nothing>>

    @POST("reses/perbaikan/laporan-reses")
    fun perbaikanHasilReses(
        @Query("reses_id") rapatId: Int,
        @Query("message") message: String
    ): Single<StandardResponse<Nothing>>
}