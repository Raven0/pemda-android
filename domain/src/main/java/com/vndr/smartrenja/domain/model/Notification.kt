package com.vndr.smartrenja.domain.model

import androidx.annotation.Keep
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import java.io.Serializable

@Keep
data class Notification(
    @PrimaryKey
    @SerializedName("id") val id: String?,
    @SerializedName("content") val content: Content?,
    @SerializedName("created_at") val createdAt: String?,
    @SerializedName("updated_at") val updatedAt: String?
) : Serializable

@Keep
data class Content(
    @SerializedName("title") val title: String?,
    @SerializedName("body") val body: String?,
    @SerializedName("akd_name") val akdName: String?,
    @SerializedName("tanggal") val tanggal: String?,
    @SerializedName("kegiatan") val kegiatan: String?
) : Serializable
