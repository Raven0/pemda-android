package com.vndr.smartrenja.domain.source.local

import android.content.Context
import android.content.SharedPreferences
import com.vndr.smartrenja.domain.R
import javax.inject.Inject

class PrefManager @Inject constructor(val context: Context) {

    private val pref: SharedPreferences by lazy {
        context.getSharedPreferences(
            context.getString(R.string.preference_file_key), Context.MODE_PRIVATE
        )
    }

    fun getLogin() = pref.getBoolean(PREF_KEY_LOGIN, false)
    fun setLogin(boolean: Boolean) = pref.edit().putBoolean(PREF_KEY_LOGIN, boolean).commit()

    fun getFirebaseToken(): String? = pref.getString(PREF_KEY_FIREBASE_TOKEN, null)
    fun setFirebaseToken(token: String) =
        pref.edit().putString(PREF_KEY_FIREBASE_TOKEN, token).commit()

    fun getDeviceUuid() = pref.getString(PREF_KEY_UUID, null)
    fun setDeviceUUid(uuid: String) = pref.edit().putString(PREF_KEY_UUID, uuid).commit()

    fun getMenuAkd() = pref.getString(PREF_KEY_MENU_AKD, null)
    fun setMenuAkd(akd: String) = pref.edit().putString(PREF_KEY_MENU_AKD, akd).commit()

    companion object {
        private const val PREF_KEY_UUID = "device_uuid"
        private const val PREF_KEY_FIREBASE_TOKEN = "firebase_token"
        private const val PREF_KEY_LOGIN = "login"
        private const val PREF_KEY_MENU_AKD = "menu_akd"
    }
}