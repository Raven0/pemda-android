package com.vndr.smartrenja.domain.source.remote

import android.util.Log
import com.vndr.smartrenja.domain.source.local.dao.TokenDao
import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response
import javax.inject.Inject

class AuthInterceptor @Inject constructor(
    private val tokenDao: TokenDao
) : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        var request = chain.request()

        try {
            val accessToken = tokenDao.getToken().blockingGet()
            Log.d(this::class.java.name, "====> token: ${accessToken.token}")
            request = request.newBuilder()
                .addHeaders(accessToken.token)
                .url(request.url)
                .build()
        } catch (ignored: Exception) { }

        return chain.proceed(request)
    }

    private fun Request.Builder.addHeaders(token: String) =
        this.apply { header("Authorization", "Bearer $token") }
}