package com.vndr.smartrenja.domain.base

import android.util.Log
import com.vndr.smartrenja.domain.model.response.StandardResponse
import com.vndr.smartrenja.domain.source.model.exceptions.EmptyDataException
import com.vndr.smartrenja.domain.source.model.state.Resource
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.*
import io.reactivex.rxjava3.schedulers.Schedulers

/*
    Developed by Ismail Rohaga
    Linkedin: https://www.linkedin.com/in/ismailrohaga/
 */

open class BaseRepository {
    fun <T> Single<StandardResponse<T>>.prepareNetworkRequest(): Single<Resource<T>> = this.compose(
        SingleTransformer { source ->
            source.map {
                return@map if (it.statusCode == "OK") Resource.Success(it.data)
                else Resource.Error.ApiError(it.message, it.code)
            }.onErrorResumeNext {
                Log.d(this::class.java.name, "---> prepareNetworkRequest: Error $it")
                Single.just(Resource.Error.Thrown(it))
            }
        }
    )

    fun <T> Observable<StandardResponse<T>>.prepareNetworkRequest(): Observable<Resource<T>> =
        this.compose(ObservableTransformer { source ->
            source.flatMap {
                return@flatMap Observable.just(
                    if (it.statusCode == "OK") Resource.Success(it.data)
                    else Resource.Error.ApiError(it.message, it.code)
                )
            }.onErrorResumeNext { Observable.just(Resource.Error.Thrown(it)) }
        })

    fun <T> Flowable<List<T>>.prepareLocalFlowableRequest(): Flowable<Resource<T>> = this.compose(
        FlowableTransformer { source ->
            source.flatMap {
                return@flatMap Flowable.just(
                    if (it == null || (it.isEmpty())) {
                        Resource.Error.Thrown(EmptyDataException())
                    } else Resource.Success(it.first())
                )
            }.onErrorResumeNext { Flowable.just(Resource.Error.Thrown(it)) }
        }
    )

    fun <T> Flowable<T>.prepareLocalRequest(): Flowable<Resource<T>> = this.compose(
        FlowableTransformer { source ->
            source.flatMap {
                return@flatMap Flowable.just(
                    if (it == null || (it is List<*> && it.isEmpty())) {
                        Resource.Error.Thrown(EmptyDataException())
                    } else Resource.Success(it)
                )
            }.onErrorResumeNext { Flowable.just(Resource.Error.Thrown(it)) }
        }
    )

    fun <T> Single<T>.prepareLocalRequest(): Single<Resource<T>> = this.compose { source ->
        source.flatMap {
            Log.d(this::class.java.name, "---> prepareLocalRequest: $it")
            return@flatMap Single.just(
                if (it == null || (it is List<*> && (it as List<*>).size == 0)) Resource.Error.Thrown(
                    EmptyDataException()
                )
                else Resource.Success<T>(it)
            )
        }.onErrorResumeNext {
            Log.e(this::class.java.name, "---> onErrorResumeNext: $it")
            Single.just(Resource.Error.Thrown(it))
        }
    }

    fun <T> Single<T>.addSchedulers(): Single<T> = this.compose(addSchedulersSingle())

    fun <T> Observable<T>.addSchedulers(): Observable<T> = this.compose(addSchedulersObservable())

    fun <T> Flowable<T>.addSchedulers(): Flowable<T> = this.compose(addScedulersFlowable())

    fun Completable.addSchedulers(): Completable = this.compose(addSchedulersCompletable())

    private fun <T> addSchedulersSingle(): SingleTransformer<T, T> = SingleTransformer {
        return@SingleTransformer it
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

    private fun <T> addSchedulersObservable(): ObservableTransformer<T, T> = ObservableTransformer {
        return@ObservableTransformer it
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

    private fun <T> addScedulersFlowable(): FlowableTransformer<T, T> = FlowableTransformer {
        return@FlowableTransformer it
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

    private fun addSchedulersCompletable(): CompletableTransformer = CompletableTransformer {
        return@CompletableTransformer it
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

    fun <T> getCache(cacheOperation: CacheOperation<T>): Flowable<Resource<T>> {
        val getCacheBuilder = GetCacheBuilder(cacheOperation)
        return getCacheBuilder.local()
    }

    inner class GetCacheBuilder<T> constructor(
        private val cacheOperation: CacheOperation<T>
    ) {

        var page = 1
        fun local(): Flowable<Resource<T>> {
            return cacheOperation.local().flatMap {
                if (it is Resource.Error.Thrown && it.exception is EmptyDataException) {
                    page = 1
                    return@flatMap network()
                }

                return@flatMap Flowable.just(it)

            }.addSchedulers().onBackpressureBuffer()
        }

        private fun network(): Flowable<Resource<T>> {
            return cacheOperation.network(page).flatMapPublisher {
                if (it is Resource.Success) {
                    if (cacheOperation.isPagination()) {
                        page++
                        return@flatMapPublisher cacheOperation.insert(it.data!!).addSchedulers()
                            .andThen(network())
                    } else
                        return@flatMapPublisher cacheOperation.insert(it.data!!).addSchedulers()
                            .andThen(Flowable.just(it))
                }
                return@flatMapPublisher Flowable.just(it)
            }
        }
    }
}

interface CacheOperation<T> {
    fun local(): Flowable<Resource<T>>

    fun isPagination(): Boolean

    fun network(page: Int? = null): Single<Resource<T>>

    fun insert(data: T): Completable
}