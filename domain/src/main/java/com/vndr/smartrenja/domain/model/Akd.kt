package com.vndr.smartrenja.domain.model

import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName
import java.io.Serializable

@Keep
data class Akd(
    @SerializedName("id") val id: Int?,
    @SerializedName("akd_id") val akdId: Int?,
    @SerializedName("nama_akd") val namaAkd: String?,
    @SerializedName("program_kerja") val programKerja: List<ProgramKerja>?,
    @SerializedName("can_approve") val canApprove: Boolean?
) : Serializable