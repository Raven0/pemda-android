package com.vndr.smartrenja.domain.source.repo

import com.vndr.smartrenja.domain.base.BaseRepository
import com.vndr.smartrenja.domain.model.*
import com.vndr.smartrenja.domain.source.model.state.Resource
import com.vndr.smartrenja.domain.source.remote.ApiService
import io.reactivex.rxjava3.core.Single
import javax.inject.Inject

class KegiatanRepository @Inject constructor(
    private val apiService: ApiService,
) : BaseRepository() {
    fun getSchedule(progja: Int?, akd: Int?, tahun: Int?): Single<Resource<List<Schedule>>> =
        apiService.getSchedule(progja, akd, tahun)
            .prepareNetworkRequest()
            .addSchedulers()

    fun getUpcomingSchedule(): Single<Resource<List<Schedule>>> =
        apiService.getUpcomingSchedule()
            .prepareNetworkRequest()
            .addSchedulers()

    fun getRapat(id: Int): Single<Resource<List<Rapat>>> = apiService.getRapat(id)
        .prepareNetworkRequest()
        .addSchedulers()

    fun getRapatDetail(id: Int): Single<Resource<Rapat>> = apiService.getRapatDetail(id)
        .prepareNetworkRequest()
        .addSchedulers()

    fun getKunker(id: Int): Single<Resource<List<Kunker>>> = apiService.getKunker(id)
        .prepareNetworkRequest()
        .addSchedulers()

    fun getKunkerDetail(id: Int): Single<Resource<Kunker>> = apiService.getKunkerDetail(id)
        .prepareNetworkRequest()
        .addSchedulers()

    fun getReses(id: Int): Single<Resource<List<Reses>>> = apiService.getReses(id)
        .prepareNetworkRequest()
        .addSchedulers()

    fun getResesDetail(id: Int): Single<Resource<Reses>> = apiService.getResesDetail(id)
        .prepareNetworkRequest()
        .addSchedulers()

    fun getRenja(progja: Int, akd: Int, tahun: Int): Single<Resource<List<Renja>>> =
        apiService.getRenjaByProker(progja, akd, tahun)
            .prepareNetworkRequest()
            .addSchedulers()
}

