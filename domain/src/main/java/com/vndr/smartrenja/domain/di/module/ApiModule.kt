package com.vndr.smartrenja.domain.di.module

import com.vndr.smartrenja.domain.di.qualifier.RetrofitAuthQualifier
import com.vndr.smartrenja.domain.source.remote.ApiService
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
object ApiModule {
    @Provides
    @Singleton
    fun provideApiService(@RetrofitAuthQualifier retrofit: Retrofit): ApiService {
        return retrofit.create(ApiService::class.java)
    }
}