package com.vndr.smartrenja.domain.di.component

import android.app.Application
import android.content.Context
import com.google.gson.Gson
import com.vndr.smartrenja.domain.di.module.*
import com.vndr.smartrenja.domain.source.local.PrefManager
import com.vndr.smartrenja.domain.source.repo.*
import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton

/*
    Developed by Ismail Rohaga
    Linkedin: https://www.linkedin.com/in/ismailrohaga/
 */

@Component(
    modules = [
        ContextModule::class,
        RetrofitModule::class,
        DatabaseModule::class,
        DaoModule::class,
        ApiModule::class,
        PrefManagerModule::class,
        RepositoryModule::class
    ]
)
@Singleton
interface CoreComponent {
    //put here anything you need from module for activityScope component
    @Component.Factory
    interface Factory{
        fun create(@BindsInstance application: Application): CoreComponent
    }

    fun context(): Context

    fun gson(): Gson

    fun prefManager(): PrefManager

    // add repository inject here

    fun anggotaRepository(): AnggotaRepository

    fun approvalRepository(): ApprovalRepository

    fun authRepository(): AuthRepository

    fun bannerRepository(): BannerRepository

    fun faqsRepository(): FaqsRepository

    fun kegiatanRepository(): KegiatanRepository

    fun notificationRepository(): NotificationRepository

    fun userRepository(): UserRepository
}