package com.vndr.smartrenja.domain.source.repo

import com.vndr.smartrenja.domain.base.BaseRepository
import com.vndr.smartrenja.domain.model.Notification
import com.vndr.smartrenja.domain.source.model.state.Resource
import com.vndr.smartrenja.domain.source.remote.ApiService
import io.reactivex.rxjava3.core.Single
import javax.inject.Inject

class NotificationRepository @Inject constructor(
    private val apiService: ApiService,
) : BaseRepository() {
    fun getNotifications(): Single<Resource<List<Notification>>> = apiService.getNotifications()
        .prepareNetworkRequest()
        .addSchedulers()

    fun clearNotification(): Single<Resource<Nothing>> = apiService.clearNotifications()
        .prepareNetworkRequest()
        .addSchedulers()

    fun readNotifications(id: String): Single<Resource<Nothing>> = apiService.readNotifications(id)
        .prepareNetworkRequest()
        .addSchedulers()
}

