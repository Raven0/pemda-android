package com.vndr.smartrenja.domain.source.model.request

import com.google.gson.annotations.SerializedName

data class LoginRequest(
    @SerializedName("username") val username: String,
    @SerializedName("password")val password: String
)
