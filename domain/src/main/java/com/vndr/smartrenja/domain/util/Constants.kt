package com.vndr.smartrenja.domain.util

object Constants {
    const val BASE_URL = "https://sirajasmart.dprd.bandungbaratkab.go.id/api/"
    const val PERENCANAAN = "PERENCANAAN"
    const val PELAKSANAAN = "PELAKSANAAN"
    const val PELAPORAN = "PELAPORAN"
    const val RAPAT = "RAPAT"
    const val RESES = "RESES"
    const val KUNKER = "KUNKER"
    const val ACTION_VIEW = "VIEW"
    const val ACTION_APPROVE = "APPROVE"
    const val ACTION_REJECT = "REJECT"
    const val INTENT_AKD_ID = "INTENT_AKD_ID"
    const val INTENT_AKD_NAME = "INTENT_AKD_NAME"
    const val INTENT_PROGJA_ID = "INTENT_PROGJA_ID"
    const val INTENT_PROGJA_NAME = "INTENT_PROGJA_NAME"
    const val INTENT_RENJA_ID = "INTENT_RENJA_ID"
    const val INTENT_RENJA_NAME = "INTENT_RENJA_NAME"
    const val INTENT_KEGIATAN_ID = "INTENT_KEGIATAN_ID"
    const val INTENT_KEGIATAN_TYPE = "INTENT_KEGIATAN_TYPE"
    const val TYPE_ALL = "TYPE_ALL"
    const val TYPE_RAPAT = "TYPE_RAPAT"
    const val TYPE_KUNKER = "TYPE_KUNKER"
    const val TYPE_RESES = "TYPE_RESES"
}

