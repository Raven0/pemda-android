package com.vndr.smartrenja.domain.util

import android.annotation.SuppressLint
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

object DateUtils {

    @SuppressLint("SimpleDateFormat")
    fun convertToDate(date: String?): Date? {
        return try {
            val toConvert = date?.replaceAfter(".", "")
            if (toConvert != null) {
                SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(
                    toConvert.substring(
                        0,
                        toConvert.length - 1
                    )
                )
            } else {
                null
            }
        } catch (e: ParseException) {
            null
        }
    }

    fun convertEpochToDate(epochTime: Long): String {
        val date = Date(epochTime)
        return SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US).format(date)
    }
}

fun String.toDate(pattern: String): Date? =
    SimpleDateFormat(pattern, Locale.getDefault()).parse(this)

fun Date.reformat(format: String): String =
    SimpleDateFormat(format, Locale.getDefault()).format(this)
