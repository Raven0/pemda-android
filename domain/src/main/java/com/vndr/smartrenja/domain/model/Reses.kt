package com.vndr.smartrenja.domain.model

import androidx.annotation.Keep
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import com.vndr.smartrenja.domain.model.Files
import com.vndr.smartrenja.domain.model.Gallery
import com.vndr.smartrenja.domain.model.Peserta
import java.io.Serializable
import java.time.LocalDate

@Keep
data class Reses(
    @PrimaryKey
    @SerializedName("id") val id: Int?,
    @SerializedName("rencana_kerja_id") val rencanaKerjaId: String?,
    @SerializedName("rencana_kerja") val rencanaKerja: String?,
    @SerializedName("program_kerja_id") val programKerjaId: Int?,
    @SerializedName("program_kerja") val programKerja: String?,
    @SerializedName("dasar_reses") val dasarReses: String?,
    @SerializedName("pelaksana_reses") val pelaksanaReses: String?,
    @SerializedName("dapil") val dapil: String?,
    @SerializedName("lokasi_reses") val lokasi_reses: String?,
    @SerializedName("masa_sidang") val masa_sidang: String?,
    @SerializedName("tanggal") val tanggal: String?,
    @SerializedName("tahun_sidang") val tahun_sidang: String?,
    @SerializedName("tanggal_awal") val tanggal_awal: String?,
    @SerializedName("tanggal_akhir") val tanggal_akhir: String?,
    @SerializedName("kepala_bagian") val kepala_bagian: String?,
    @SerializedName("ketua_dprd") val ketua_dprd: String?,
    @SerializedName("sekretariat") val sekretariat: String?,
    @SerializedName("created_at") val createdAt: String?,
    @SerializedName("updated_at") val updatedAt: String?,
    @SerializedName("is_approve_materi_reses") val isApproveMateriReses: Boolean?,
    @SerializedName("status_materi_reses") val statusMateriRapat: String?,
    @SerializedName("note_materi_reses") val noteMateriReses: String?,
    @SerializedName("is_approve_laporan_reses") val isApproveLaporanReses: Boolean?,
    @SerializedName("status_laporan_reses") val statusLaporanRapat: String?,
    @SerializedName("note_laporan_reses") val noteLaporanReses: String?,
    @SerializedName("peserta") val peserta: List<Peserta>?,
    @SerializedName("galleries") val galleries: List<Gallery>?,
    @SerializedName("files") val files: Files?,
    val date: LocalDate
) : Serializable