package com.vndr.smartrenja.domain.source.repo

import com.vndr.smartrenja.domain.base.BaseRepository
import com.vndr.smartrenja.domain.model.Anggota
import com.vndr.smartrenja.domain.source.model.state.Resource
import com.vndr.smartrenja.domain.source.remote.ApiService
import io.reactivex.rxjava3.core.Single
import javax.inject.Inject

class AnggotaRepository @Inject constructor(
    private val apiService: ApiService,
) : BaseRepository() {
    fun getAnggota(akd: Int): Single<Resource<List<Anggota>>> = apiService.getAnggotaByAkd(akd)
        .prepareNetworkRequest()
        .addSchedulers()
}

