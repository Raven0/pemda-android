package com.vndr.smartrenja.domain.source.local

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.vndr.smartrenja.domain.source.local.dao.TokenDao
import com.vndr.smartrenja.domain.source.local.dao.UserDao
import com.vndr.smartrenja.domain.source.model.Token
import com.vndr.smartrenja.domain.source.model.User

@Database(
    entities = [User::class, Token::class],
    version = 1, exportSchema = false
)
@TypeConverters(User.DataConverters::class)
abstract class SmartRenjaDb : RoomDatabase() {

    abstract fun getAccountDao(): UserDao
    abstract fun getAccessTokenDao(): TokenDao
    companion object {
        const val DB_NAME = "smartrenja_db"
    }

}