package com.vndr.smartrenja.domain.source.model.request

import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName
@Keep
data class LogoutRequest(
    @SerializedName("device_uuid") val uuid: String?
)
