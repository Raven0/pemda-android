package com.vndr.smartrenja.domain.model

import androidx.annotation.Keep
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import java.io.Serializable

@Keep
data class Renja(
    @PrimaryKey
    @SerializedName("id") val id: Int?,
    @SerializedName("nama") val nama: String?,
    @SerializedName("tahun") val tahun: String?,
    @SerializedName("volume") val volume: String?,
    @SerializedName("target") val target: String?,
    @SerializedName("indikator") val indikator: String?,
    @SerializedName("file_laporan_progja") val fileLaporanProgja: String?,
    @SerializedName("program_kerja_id") val programKerjaId: Int?,
    @SerializedName("created_at") val createdAt: String?,
    @SerializedName("updated_at") val updatedAt: String?,
    @SerializedName("master_akd_id") val masterAkdId: Int?,
    @SerializedName("is_approve_laporan_progja") val isApproveLaporanProgja: String?,
    @SerializedName("status_laporan_progja") val statusLaporanProgja: String?,
    @SerializedName("catatan") val catatan: String?
) : Serializable