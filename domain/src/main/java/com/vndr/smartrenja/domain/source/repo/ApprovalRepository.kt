package com.vndr.smartrenja.domain.source.repo

import com.vndr.smartrenja.domain.base.BaseRepository
import com.vndr.smartrenja.domain.model.Anggota
import com.vndr.smartrenja.domain.source.model.state.Resource
import com.vndr.smartrenja.domain.source.remote.ApiService
import io.reactivex.rxjava3.core.Single
import javax.inject.Inject

class ApprovalRepository @Inject constructor(
    private val apiService: ApiService,
) : BaseRepository() {
    fun approveMateriRapat(id: Int): Single<Resource<Nothing>> = apiService.approveMateriRapat(id)
        .prepareNetworkRequest()
        .addSchedulers()

    fun approveMateriKunker(id: Int): Single<Resource<Nothing>> = apiService.approveMateriKunker(id)
        .prepareNetworkRequest()
        .addSchedulers()

    fun approveMateriReses(id: Int): Single<Resource<Nothing>> = apiService.approveMateriReses(id)
        .prepareNetworkRequest()
        .addSchedulers()

    fun approveHasilRapat(id: Int): Single<Resource<Nothing>> = apiService.approveHasilRapat(id)
        .prepareNetworkRequest()
        .addSchedulers()

    fun approveHasilReses(id: Int): Single<Resource<Nothing>> = apiService.approveHasilReses(id)
        .prepareNetworkRequest()
        .addSchedulers()

    fun approveLaporanKunker(id: Int): Single<Resource<Nothing>> =
        apiService.approveLaporanKunker(id)
            .prepareNetworkRequest()
            .addSchedulers()

    fun approveLaporanProgja(id: Int): Single<Resource<Nothing>> =
        apiService.approveLaporanProgja(id)
            .prepareNetworkRequest()
            .addSchedulers()

    fun rejectMateriRapat(id: Int, message: String): Single<Resource<Nothing>> =
        apiService.perbaikanMateriRapat(id, message)
            .prepareNetworkRequest()
            .addSchedulers()

    fun rejectMateriKunker(id: Int, message: String): Single<Resource<Nothing>> =
        apiService.perbaikanMateriKunker(id, message)
            .prepareNetworkRequest()
            .addSchedulers()

    fun rejectMateriReses(id: Int, message: String): Single<Resource<Nothing>> =
        apiService.perbaikanMateriReses(id, message)
            .prepareNetworkRequest()
            .addSchedulers()

    fun rejectHasilRapat(id: Int, message: String): Single<Resource<Nothing>> =
        apiService.perbaikanHasilRapat(id, message)
            .prepareNetworkRequest()
            .addSchedulers()

    fun rejectHasilReses(id: Int, message: String): Single<Resource<Nothing>> =
        apiService.perbaikanHasilReses(id, message)
            .prepareNetworkRequest()
            .addSchedulers()

    fun rejectLaporanKunker(id: Int, message: String): Single<Resource<Nothing>> =
        apiService.perbaikanLaporanKunker(id, message)
            .prepareNetworkRequest()
            .addSchedulers()

    fun rejectLaporanProgja(id: Int, message: String): Single<Resource<Nothing>> =
        apiService.perbaikanLaporanProgja(id, message)
            .prepareNetworkRequest()
            .addSchedulers()
}

