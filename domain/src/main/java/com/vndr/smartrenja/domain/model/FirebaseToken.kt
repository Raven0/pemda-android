package com.vndr.smartrenja.domain.model

import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName
import java.io.Serializable

@Keep
data class FirebaseToken(
    @SerializedName("your_token") val token: String?
) : Serializable