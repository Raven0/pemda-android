package com.vndr.smartrenja.extention

import android.app.Activity
import android.content.Context
import android.content.ContextWrapper
import android.content.Intent
import android.net.Uri
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment

fun Activity.startActivity(destination: Class<*>) =
    startActivity(Intent(this, destination))

fun Activity.openWebPage(link: String) = startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(link)))

fun Activity?.getWorkingDirectory(): String = ContextWrapper(this).filesDir.absolutePath

fun AppCompatActivity?.openFragment(container: Int, fragment: Fragment) {
    val transaction = this?.supportFragmentManager?.beginTransaction()
    transaction?.add(container, fragment)
    transaction?.addToBackStack(fragment.tag)
    transaction?.commit()
}

fun Context?.toast(value: () -> String) = Toast.makeText(this, value(), Toast.LENGTH_SHORT).show()

fun Fragment?.toast(value: () -> String) = Toast.makeText(this?.requireContext(), value(), Toast.LENGTH_SHORT).show()