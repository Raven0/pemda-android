package com.vndr.smartrenja.extention

import com.vndr.smartrenja.base.BaseApplication
import com.vndr.smartrenja.di.component.*

val viewModelInjection: ViewModelComponent
    get() = DaggerViewModelComponent.factory().create(BaseApplication.appComponent!!)

val serviceInjection: ServiceComponent
    get() = DaggerServiceComponent.factory().create(BaseApplication.appComponent!!)

val activityInjection: ActivityComponent
    get() = DaggerActivityComponent.factory().create(BaseApplication.appComponent!!)
