package com.vndr.smartrenja.base

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.vndr.smartrenja.domain.source.model.state.Resource
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Flowable
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.ObservableSource
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.disposables.Disposable
import javax.inject.Inject

abstract class BaseViewModel : ViewModel() {
    @Inject
    lateinit var compositeDisposable: CompositeDisposable

    fun <T> Observable<Resource<T>>.subscribeUsing(liveData: MutableLiveData<   Resource<T>>, onSuccess: ((resource: Resource.Success<T>) -> Unit)? = null): Disposable =
        this.startWith(ObservableSource { Resource.Loading })
            .subscribe {
                if (it is Resource.Success) onSuccess?.invoke(it)
                liveData.postValue(it)
            }

    fun Completable.subscribeUsing(liveData: MutableLiveData<Resource<Boolean>>): Disposable =
        this.doOnSubscribe { liveData.postValue(Resource.Loading) }
            .subscribe({
                liveData.postValue(Resource.Success(true))
            }, {
                Log.e(this::class.java.name, "---> subscribeUsing Error: $it")
                liveData.postValue((Resource.Error.Thrown(it)))
            })

    fun <T> Single<Resource<T>>.subscribeUsing(liveData: MutableLiveData<Resource<T>>, onSuccess: ((resource: Resource.Success<T>) -> Unit)? = null): Disposable =
        this.doOnSubscribe { liveData.postValue(Resource.Loading) }
            .subscribe({
                if (it is Resource.Success) onSuccess?.invoke(it)
                liveData.postValue(it)
            }, {
                Log.e(this::class.java.name, "---> subscribeUsing Error: $it")
                Log.e(this::class.java.name, "---> subscribeUsing Error: ${it.message}")
                liveData.postValue(Resource.Error.Thrown(it))
            })

    fun <T> Flowable<Resource<T>>.subscribeUsing(liveData: MutableLiveData<Resource<T>>, onSubscribe: ((Resource: Resource<T>) -> Unit)? = null) : Disposable =
        this.doOnSubscribe { liveData.postValue(Resource.Loading) }
            .subscribe({
                onSubscribe?.invoke(it)
                liveData.postValue(it)
            }, {
                Log.e(this::class.java.name, "---> subscribeUsing Error: $it")
                Log.e(this::class.java.name, "---> subscribeUsing Error: ${it.message}")
                liveData.postValue(Resource.Error.Thrown(it))
            })

    override fun onCleared() {
        if (!compositeDisposable.isDisposed) compositeDisposable.dispose()
        super.onCleared()
    }

}