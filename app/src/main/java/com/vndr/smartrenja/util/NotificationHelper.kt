package com.vndr.smartrenja.util

import android.annotation.SuppressLint
import android.app.*
import android.app.PendingIntent.*
import android.content.Context
import android.content.Intent
import android.media.RingtoneManager
import android.os.Build
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat
import com.vndr.smartrenja.R
import java.util.*

@SuppressLint("UnspecifiedImmutableFlag")
object NotificationHelper {

    @RequiresApi(Build.VERSION_CODES.O)
    fun createNotificationChannel(
        context: Context,
        importance: Int,
        showBadge: Boolean,
        name: String,
        description: String
    ) {
        val channel = NotificationChannel(name, name, importance)
        channel.description = description
        channel.setShowBadge(showBadge)
        val notificationManager = context.getSystemService(NotificationManager::class.java)
        notificationManager.createNotificationChannel(channel)
    }

    fun sendNotification(
        context: Context,
        notificationId: Int,
        notificationChannel: String?,
        title: String,
        body: String,
        intent: Intent?
    ) {
        val channelId = notificationChannel ?: context.getString(R.string.app_name)

        val pendingIntent =
            getActivity(context, 0, intent, FLAG_ONE_SHOT)

        val notificationManager =
            context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        notificationManager.notify(
            notificationId,
            buildNotification(
                context,
                title,
                body,
                pendingIntent,
                channelId
            ).build()
        )
    }

    private fun buildNotification(
        context: Context, title: String?,
        body: String?, pendingIntent: PendingIntent, channelId: String
    ): NotificationCompat.Builder = NotificationCompat.Builder(context, channelId)
        //.setSmallIcon(R.drawable.ic_notification_icon)
        //.setColor(ContextCompat.getColor(context, R.color.accent))
        .setContentTitle(title)
        .setContentText(body)
        .setAutoCancel(true)
        .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
        .setContentIntent(pendingIntent)

}