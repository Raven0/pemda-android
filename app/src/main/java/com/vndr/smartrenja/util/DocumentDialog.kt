package com.vndr.smartrenja.util

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.view.Window
import android.widget.ImageView
import android.widget.Space
import android.widget.TextView
import androidx.appcompat.widget.AppCompatButton
import com.google.android.material.textfield.TextInputEditText
import com.vndr.smartrenja.R
import com.vndr.smartrenja.domain.util.Constants.ACTION_APPROVE
import com.vndr.smartrenja.domain.util.Constants.ACTION_REJECT
import com.vndr.smartrenja.domain.util.Constants.ACTION_VIEW
import com.vndr.smartrenja.extention.makeGone

/**
 * Created by IsmailR on 1/27/21.
 */
class DocumentDialog(private val context: Context) {
    var onDialogClick: ((String) -> Unit)? = null
    var onFeedbackDialogClick: ((String) -> Unit)? = null

    @SuppressLint("SetTextI18n")
    fun showDialog(title: String, status: String, message: String?) {
        val dialog = Dialog(context)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(true)
        dialog.setContentView(R.layout.layout_document_dialog)

        val tvTitle = dialog.findViewById(R.id.tvTitle) as TextView
        val tvStatus = dialog.findViewById(R.id.tvStatus) as TextView
        val tvMessageTitle = dialog.findViewById(R.id.tvMessageTitle) as TextView
        val tvMessage = dialog.findViewById(R.id.tvMessage) as TextView
        val btnView = dialog.findViewById(R.id.btnView) as AppCompatButton
        val btnApprove = dialog.findViewById(R.id.btnApprove) as AppCompatButton
        val btnDisapprove = dialog.findViewById(R.id.btnDissaprove) as AppCompatButton
        val btnClose = dialog.findViewById(R.id.btnClose) as ImageView
        val space = dialog.findViewById(R.id.space_around) as Space

        tvTitle.text = title
        tvStatus.text = "Status: $status"

        if (message == null){
            tvMessageTitle.makeGone()
            tvMessage.makeGone()
        }else{
            tvMessage.text = "$message"
        }

        if (status == "diterima"){
            btnApprove.makeGone()
            space.makeGone()
        }

        btnView.setOnClickListener {
            dialog.dismiss()
            onDialogClick?.invoke(ACTION_VIEW)
        }

        btnApprove.setOnClickListener {
            dialog.dismiss()
            onDialogClick?.invoke(ACTION_APPROVE)
        }

        btnDisapprove.setOnClickListener {
            dialog.dismiss()
            onDialogClick?.invoke(ACTION_REJECT)
        }

        btnClose.setOnClickListener {
            dialog.dismiss()
        }

        dialog.show()
    }

    fun showFeedbackDialog() {
        val dialog = Dialog(context)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(true)
        dialog.setContentView(R.layout.layout_feedback_dialog)

        val etFeedback = dialog.findViewById(R.id.etFeedback) as TextInputEditText
        val btnSubmit = dialog.findViewById(R.id.btnSubmit) as AppCompatButton
        val btnClose = dialog.findViewById(R.id.btnClose) as ImageView

        btnSubmit.setOnClickListener {
            dialog.dismiss()
            onFeedbackDialogClick?.invoke(etFeedback.text.toString())
        }

        btnClose.setOnClickListener {
            dialog.dismiss()
        }

        dialog.show()
    }
}