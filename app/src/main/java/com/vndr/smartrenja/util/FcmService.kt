package com.vndr.smartrenja.util

import com.google.firebase.ktx.Firebase
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.google.firebase.messaging.ktx.messaging
import com.vndr.smartrenja.domain.source.local.PrefManager
import com.vndr.smartrenja.domain.source.repo.AuthRepository
import javax.inject.Inject

class FcmService : FirebaseMessagingService() {

    @Inject
    lateinit var prefManager: PrefManager

    @Inject
    lateinit var authRepository: AuthRepository

    override fun onCreate() {
        super.onCreate()
    }

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        if (remoteMessage.data.isNotEmpty()) handlePayload(remoteMessage)
        else remoteMessage.notification?.let {
            /*
            NotificationHelper.sendNotification(
                this,
                it.hashCode(),
                it.channelId,
                it.title.toString(),
                it.body.toString(),
                Intent(this, SplashActivity::class.java).startAtTop()
            )

             */
        }
    }

    override fun onNewToken(token: String) {
        super.onNewToken(token)
        prefManager.setFirebaseToken(token)
        authRepository.saveToken() // todo: need to check if user is logged
    }

    private fun handlePayload(remoteMessage: RemoteMessage) {
        val data = remoteMessage.data
        /*
        is_showing will be used if there's a case where sometimes we want to show the notification
        or just straight execute the command.

        val isShowing = data.getValue("is_showing") == "true" //data["is_showing"].toBoolean()
        */

        when (data.getValue("action")) {
            /*
            OPEN_TRANSACTION_ACTION -> {
                val intent = Intent(this, TransactionDetailsActivity::class.java)
                    .putExtras(Bundle().apply {
                        putString(
                            TransactionDetailsActivity.TRANSACTION_OBJECT,
                            data.getValue("payload")
                        )
                    })

                remoteMessage.notification?.let {
                    showNotification(this, it, intent.startAtTop())
                }
            }

             */

                 /*
            DEEPLINK_ACTION -> {
                val intent = Intent(this, SplashActivity::class.java)
                    .setData(Uri.parse(data.getValue("payload"))).startAtTop()

                remoteMessage.notification?.let {
                    showNotification(this, it, intent)
                }
            }

            else -> remoteMessage.notification?.let {
                showNotification(this, it, null)
            }

                  */

        }
    }

    companion object {
        // open page with payload
        private const val DEEPLINK_ACTION = "deeplink"

        /*
        fun showNotification(
            context: Context,
            notification: RemoteMessage.Notification,
            intent: Intent?
        ) {
            NotificationHelper.sendNotification(
                context,
                notification.hashCode(),
                notification.channelId,
                notification.title.toString(),
                notification.body.toString(),
                intent ?: Intent(context, SplashActivity::class.java).startAtTop()
            )
        }

         */

        fun subscribeTopic(name: String, isSubscribe: Boolean) {
            if (isSubscribe) Firebase.messaging.subscribeToTopic(name)
            else Firebase.messaging.unsubscribeFromTopic(name)
        }
    }
}