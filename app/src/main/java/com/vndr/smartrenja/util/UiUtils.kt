package com.vndr.smartrenja.util

import android.content.Intent
import android.content.res.Resources
import android.view.View
import androidx.viewpager.widget.ViewPager
import java.util.*

fun Intent.startAtTop() = this.apply {
    flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
}

fun ViewPager.autoSlide(duration: Long, itemSize: Int) {
    Timer().schedule(object : TimerTask() {
        override fun run() {
            var position = currentItem
            if (position == itemSize - 1) position = -1

            post { setCurrentItem(position + 1, true) }
        }
    }, duration, duration)
}

val Int.dp: Int
    get() = (this * Resources.getSystem().displayMetrics.density + 0.5f).toInt()

fun View.setOnSafeClickListener(clicked: () -> Unit) {
    this.setOnClickListener {
        this.isClickable = false
        clicked()
        this.postDelayed({
            this@setOnSafeClickListener.isClickable = true
        },1000)
    }
}