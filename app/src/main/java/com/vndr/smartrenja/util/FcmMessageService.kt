package com.vndr.smartrenja.util

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.media.RingtoneManager
import android.os.Build
import androidx.core.app.NotificationCompat
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.vndr.smartrenja.R
import com.vndr.smartrenja.ui.activities.MainActivity

class FcmMessageService: FirebaseMessagingService() {
    override fun onNewToken(token: String) {
    }

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        super.onMessageReceived(remoteMessage)
        //Use this condition to validation login
        if(!remoteMessage.notification?.title.isNullOrBlank() && !remoteMessage.notification?.body.isNullOrBlank() && !remoteMessage.data.isNullOrEmpty()) {
            sendNotification(remoteMessage.notification.hashCode(), remoteMessage.notification!!.title.toString(), remoteMessage.notification!!.body.toString(), remoteMessage.data)
        }
    }

    private fun sendNotification(id: Int, messageTitle: String, messageBody: String, data: MutableMap<String, String>) {
        val intent = Intent(this, MainActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)


        try {
            data.getValue("notif_id").let { intent.putExtra("notif_id", it) }
            data.getValue("akd").let { intent.putExtra("akd", it) }
        } catch (x: Exception) {

        }

        intent.putExtra("id", data.getValue("id"))
        intent.putExtra("type", data.getValue("type"))
        intent.putExtra("action_request", data.getValue("action_request"))


        val pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT)

        val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)

        var notificationBuilder: NotificationCompat.Builder? = null
        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(
                    packageName,
                    packageName,
                    NotificationManager.IMPORTANCE_DEFAULT
            )
            channel.description = packageName
            notificationManager.createNotificationChannel(channel)
            if (notificationBuilder == null) {
                notificationBuilder = NotificationCompat.Builder(application, packageName)
            }
        } else {
            if (notificationBuilder == null) {
                notificationBuilder = NotificationCompat.Builder(application,packageName)
            }
        }
        notificationBuilder.setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(messageTitle)
                .setContentText(messageBody)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent)

        notificationManager.notify(id, notificationBuilder.build())
    }
}