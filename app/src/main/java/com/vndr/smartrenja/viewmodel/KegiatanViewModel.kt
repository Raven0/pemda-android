package com.vndr.smartrenja.viewmodel

import androidx.lifecycle.MutableLiveData
import com.vndr.smartrenja.base.BaseViewModel
import com.vndr.smartrenja.domain.model.*
import com.vndr.smartrenja.domain.source.model.User
import com.vndr.smartrenja.domain.source.model.state.Resource
import com.vndr.smartrenja.domain.source.repo.*
import com.vndr.smartrenja.extention.viewModelInjection
import javax.inject.Inject

class KegiatanViewModel : BaseViewModel() {

    @Inject
    lateinit var kegiatanRepository: KegiatanRepository

    @Inject
    lateinit var approvalRepository: ApprovalRepository

    val rapatLiveData by lazy { MutableLiveData<Resource<List<Rapat>>>() }
    val rapatDetailLiveData by lazy { MutableLiveData<Resource<Rapat>>() }
    val kunkerLiveData by lazy { MutableLiveData<Resource<List<Kunker>>>() }
    val kunkerDetailLiveData by lazy { MutableLiveData<Resource<Kunker>>() }
    val resesLiveData by lazy { MutableLiveData<Resource<List<Reses>>>() }
    val resesDetailLiveData by lazy { MutableLiveData<Resource<Reses>>() }

    val approveLiveData by lazy { MutableLiveData<Resource<Nothing>>() }

    init {
        viewModelInjection.inject(this)
    }

    fun loadRapat(id: Int) = compositeDisposable.add(
        kegiatanRepository.getRapat(id).subscribeUsing(rapatLiveData)
    )

    fun loadRapatDetail(id: Int) = compositeDisposable.add(
        kegiatanRepository.getRapatDetail(id).subscribeUsing(rapatDetailLiveData)
    )

    fun loadKunker(id: Int) = compositeDisposable.add(
        kegiatanRepository.getKunker(id).subscribeUsing(kunkerLiveData)
    )

    fun loadKunkerDetail(id: Int) = compositeDisposable.add(
        kegiatanRepository.getKunkerDetail(id).subscribeUsing(kunkerDetailLiveData)
    )

    fun loadReses(id: Int) = compositeDisposable.add(
        kegiatanRepository.getReses(id).subscribeUsing(resesLiveData)
    )

    fun loadResesDetail(id: Int) = compositeDisposable.add(
        kegiatanRepository.getResesDetail(id).subscribeUsing(resesDetailLiveData)
    )

    //Approval
    fun approveRapat(id: Int) = compositeDisposable.add(
        approvalRepository.approveMateriRapat(id).subscribeUsing(approveLiveData)
    )

    fun approveHasilRapat(id: Int) = compositeDisposable.add(
        approvalRepository.approveHasilRapat(id).subscribeUsing(approveLiveData)
    )

    fun approveKunker(id: Int) = compositeDisposable.add(
        approvalRepository.approveMateriKunker(id).subscribeUsing(approveLiveData)
    )

    fun approveLaporanKunker(id: Int) = compositeDisposable.add(
        approvalRepository.approveLaporanKunker(id).subscribeUsing(approveLiveData)
    )

    fun approveReses(id: Int) = compositeDisposable.add(
        approvalRepository.approveMateriReses(id).subscribeUsing(approveLiveData)
    )

    fun approveHasilReses(id: Int) = compositeDisposable.add(
        approvalRepository.approveHasilReses(id).subscribeUsing(approveLiveData)
    )

    //Reject
    fun rejectRapat(id: Int, message: String) = compositeDisposable.add(
        approvalRepository.rejectMateriRapat(id, message).subscribeUsing(approveLiveData)
    )

    fun rejectHasilRapat(id: Int, message: String) = compositeDisposable.add(
        approvalRepository.rejectHasilRapat(id, message).subscribeUsing(approveLiveData)
    )

    fun rejectKunker(id: Int, message: String) = compositeDisposable.add(
        approvalRepository.rejectMateriKunker(id, message).subscribeUsing(approveLiveData)
    )

    fun rejectLaporanKunker(id: Int, message: String) = compositeDisposable.add(
        approvalRepository.rejectLaporanKunker(id, message).subscribeUsing(approveLiveData)
    )

    fun rejectReses(id: Int, message: String) = compositeDisposable.add(
        approvalRepository.rejectMateriReses(id, message).subscribeUsing(approveLiveData)
    )

    fun rejectHasilReses(id: Int, message: String) = compositeDisposable.add(
        approvalRepository.rejectHasilReses(id, message).subscribeUsing(approveLiveData)
    )

}