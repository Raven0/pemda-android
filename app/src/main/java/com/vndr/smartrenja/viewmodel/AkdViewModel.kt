package com.vndr.smartrenja.viewmodel

import androidx.lifecycle.MutableLiveData
import com.vndr.smartrenja.base.BaseViewModel
import com.vndr.smartrenja.domain.model.*
import com.vndr.smartrenja.domain.source.model.User
import com.vndr.smartrenja.domain.source.model.state.Resource
import com.vndr.smartrenja.domain.source.repo.*
import com.vndr.smartrenja.extention.viewModelInjection
import javax.inject.Inject

class AkdViewModel : BaseViewModel() {

    @Inject
    lateinit var anggotaRepository: AnggotaRepository

    val anggotaLiveData by lazy { MutableLiveData<Resource<List<Anggota>>>() }

    init {
        viewModelInjection.inject(this)
    }

    fun loadAnggota(akd: Int) = compositeDisposable.add(
        anggotaRepository.getAnggota(akd).subscribeUsing(anggotaLiveData)
    )

}