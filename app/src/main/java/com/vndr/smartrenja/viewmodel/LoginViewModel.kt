package com.vndr.smartrenja.viewmodel

import androidx.lifecycle.MutableLiveData
import com.vndr.smartrenja.base.BaseViewModel
import com.vndr.smartrenja.domain.model.FirebaseToken
import com.vndr.smartrenja.domain.model.UserAccessToken
import com.vndr.smartrenja.domain.source.model.User
import com.vndr.smartrenja.domain.source.model.state.Resource
import com.vndr.smartrenja.domain.source.repo.AuthRepository
import com.vndr.smartrenja.extention.viewModelInjection
import javax.inject.Inject

class LoginViewModel : BaseViewModel() {
    @Inject
    lateinit var authRepository: AuthRepository

    init {
        viewModelInjection.inject(this)
    }

    val loginLiveData by lazy { MutableLiveData<Resource<User>>() }
    val tokenLiveData by lazy { MutableLiveData<Resource<FirebaseToken>>() }

    fun login(username: String, password: String, rememberMe: Boolean) = compositeDisposable.add(
        authRepository.login(username, password, rememberMe).subscribeUsing(loginLiveData)
    )

    fun saveToken() = compositeDisposable.add(
        authRepository.saveToken().subscribeUsing(tokenLiveData)
    )
}