package com.vndr.smartrenja.di.component

import com.vndr.smartrenja.di.module.CompositeDisposableModule
import com.vndr.smartrenja.di.module.FirebaseModule
import com.vndr.smartrenja.domain.di.component.CoreComponent
import com.vndr.smartrenja.domain.di.scope.CoreScope
import com.vndr.smartrenja.ui.activities.KegiatanDetailActivity
import com.vndr.smartrenja.ui.activities.MainActivity
import com.vndr.smartrenja.ui.activities.ProgjaActivity
import com.vndr.smartrenja.ui.activities.SplashActivity
import dagger.Component

@Component(dependencies = [CoreComponent::class], modules = [FirebaseModule::class,
    CompositeDisposableModule::class
])
@CoreScope
interface ActivityComponent {

    @Component.Factory
    interface Factory {
        fun create(coreComponent: CoreComponent): ActivityComponent
    }

    fun inject(splashActivity: SplashActivity)
    fun inject(mainActivity: MainActivity)
    fun inject(progjaActivity: ProgjaActivity)
    fun inject(kegiatanDetailActivity: KegiatanDetailActivity)

}