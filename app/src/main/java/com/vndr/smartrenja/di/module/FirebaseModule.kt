package com.vndr.smartrenja.di.module

import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.crashlytics.FirebaseCrashlytics
import com.google.firebase.ktx.Firebase
import com.google.firebase.messaging.ktx.messaging
import dagger.Module
import dagger.Provides

@Module
object FirebaseModule {
    @Provides
    fun provideFirebaseAnalytics() = Firebase.analytics

    @Provides
    fun provideFirebaseMessages() = Firebase.messaging

    @Provides
    fun provideFirebaseCrashlytics() = FirebaseCrashlytics.getInstance()
}