package com.vndr.smartrenja.di.component

import com.vndr.smartrenja.di.module.CompositeDisposableModule
import com.vndr.smartrenja.domain.di.component.CoreComponent
import com.vndr.smartrenja.domain.di.scope.CoreScope
import com.vndr.smartrenja.viewmodel.*
import dagger.Component

@Component(
    dependencies = [CoreComponent::class],
    modules = [
        CompositeDisposableModule::class
    ]
)
@CoreScope
interface ViewModelComponent {

    @Component.Factory
    interface Factory {
        fun create(coreComponent: CoreComponent): ViewModelComponent
    }

    fun inject(loginViewModel: LoginViewModel)
    fun inject(mainViewModel: MainViewModel)
    fun inject(akdViewModel: AkdViewModel)
    fun inject(progjaViewModel: ProgjaViewModel)
    fun inject(kegiatanViewModel: KegiatanViewModel)

}