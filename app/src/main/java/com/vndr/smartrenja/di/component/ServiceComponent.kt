package com.vndr.smartrenja.di.component

import com.vndr.smartrenja.di.module.CompositeDisposableModule
import com.vndr.smartrenja.domain.di.component.CoreComponent
import com.vndr.smartrenja.domain.di.scope.CoreScope
import com.vndr.smartrenja.ui.adapters.AkdMenuAdapter
import com.vndr.smartrenja.util.FcmService
import dagger.Component

@Component(
    dependencies = [CoreComponent::class],
    modules = [CompositeDisposableModule::class]
)
@CoreScope
interface ServiceComponent {

    @Component.Factory
    interface Factory {
        fun create(coreComponent: CoreComponent): ServiceComponent
    }

    fun inject(fcmService: FcmService)
    fun inject(akdMenuAdapter: AkdMenuAdapter)
}