package com.vndr.smartrenja.ui.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import coil.load
import coil.size.Scale
import com.smarteist.autoimageslider.SliderViewAdapter
import com.vndr.smartrenja.R
import com.vndr.smartrenja.databinding.ItemBannerBinding
import com.vndr.smartrenja.domain.model.Banner
import java.util.*

class BannerAdapter : SliderViewAdapter<BannerAdapter.ViewHolder>() {
    private var banners: MutableList<Banner> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup?): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent?.context)
        return ViewHolder(ItemBannerBinding.inflate(layoutInflater, parent, false))
    }

    fun addAll(sliders: List<Banner>) {
        for (i in sliders) {
            banners.add(i)
        }
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        viewHolder.bind(banners.distinct()[position])
    }

    override fun getCount(): Int {
        return banners.distinct().size
    }

    inner class ViewHolder(private val binding: ItemBannerBinding) :
        SliderViewAdapter.ViewHolder(binding.root) {

        fun bind(data: Banner) {
            binding.imageViewBanner.load(data.gambar) {
                scale(Scale.FILL)
                placeholder(R.drawable.bg_placeholder)
                error(R.drawable.bg_placeholder)
            }
        }
    }

}