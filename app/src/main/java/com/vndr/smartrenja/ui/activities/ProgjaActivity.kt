package com.vndr.smartrenja.ui.activities

import android.annotation.SuppressLint
import android.os.Bundle
import android.widget.ArrayAdapter
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.widget.doAfterTextChanged
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.vndr.smartrenja.R
import com.vndr.smartrenja.databinding.ActivityProgjaBinding
import com.vndr.smartrenja.domain.model.Akd
import com.vndr.smartrenja.domain.source.local.PrefManager
import com.vndr.smartrenja.domain.util.Constants
import com.vndr.smartrenja.extention.activityInjection
import com.vndr.smartrenja.ui.adapters.ProgjaPagerAdapter
import com.vndr.smartrenja.util.fromJson
import com.vndr.smartrenja.viewmodel.ProgjaViewModel
import java.util.*
import javax.inject.Inject

class ProgjaActivity : AppCompatActivity() {

    @Inject
    lateinit var prefManager: PrefManager

    private val progjaViewModel: ProgjaViewModel by viewModels()
    private val binding by lazy { ActivityProgjaBinding.inflate(layoutInflater) }

    private lateinit var bottomSheetBehavior: BottomSheetBehavior<ConstraintLayout>

    private lateinit var akd: Akd
    private var progjaId: Int = 0
    private val thisYear: Int = Calendar.getInstance().get(Calendar.YEAR)

    override fun onCreate(savedInstanceState: Bundle?) {
        activityInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        progjaId = intent.getIntExtra(Constants.INTENT_PROGJA_ID, 0).apply {
            bindByProgjaId(this)
        }
        akd = prefManager.getMenuAkd()?.fromJson<Akd>()!!

        binding.textViewTitle.text =
            intent.getStringExtra(Constants.INTENT_PROGJA_NAME)

        binding.viewPagerKegiatan.adapter = akd.canApprove?.let {
            ProgjaPagerAdapter(
                supportFragmentManager, progjaId, it
            )
        }
        binding.tabViewPager.setupWithViewPager(binding.viewPagerKegiatan)

        bottomSheetBehavior = BottomSheetBehavior.from(binding.bottomSheet)
        binding.imageButtonOption.setOnClickListener {
            if (bottomSheetBehavior.state == BottomSheetBehavior.STATE_EXPANDED)
                bottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
            else
                bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
        }

        binding.buttonFilter.setOnClickListener {
            bottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
            getData()
        }

        binding.searchInput.doAfterTextChanged {
            progjaViewModel.searchLiveData.postValue(it.toString())
        }

        binding.imageViewBack.setOnClickListener {
            onBackPressed()
        }

        bindSpinnerTahun()
        getData()
    }

    fun getData() {
        akd.akdId?.let {
            progjaViewModel.loadRenja(
                progjaId,
                it,
                binding.searchableSpinner.selectedItem.toString().toInt()
            )

            progjaViewModel.loadSchedule(
                progjaId,
                it,
                binding.searchableSpinner.selectedItem.toString().toInt()
            )
        }
    }

    private fun bindSpinnerTahun() {
        val years = ArrayList<String>()
        var currentYearIndex = 0
        for ((index, i) in (2019..thisYear + 2).withIndex()) {
            years.add(i.toString())
            if (i == thisYear) {
                currentYearIndex = index
            }
        }

        val adapterYear: ArrayAdapter<String> =
            ArrayAdapter(this, R.layout.support_simple_spinner_dropdown_item, years)
        binding.searchableSpinner.adapter = adapterYear
        binding.searchableSpinner.setSelection(currentYearIndex)
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    private fun bindByProgjaId(programKerjaId: Int) {
        when (programKerjaId) {
            1 -> {
                binding.constraintLayoutToolbar.background =
                    binding.root.context.getDrawable(R.drawable.bg_gradient_red)
            }
            3 -> {
                binding.constraintLayoutToolbar.background =
                    binding.root.context.getDrawable(R.drawable.bg_gradient_green)
            }
            2 -> {
                binding.constraintLayoutToolbar.background =
                    binding.root.context.getDrawable(R.drawable.bg_gradient_orange)
            }
            else -> {
                binding.constraintLayoutToolbar.background =
                    binding.root.context.getDrawable(R.drawable.bg_gradient_blue)
            }
        }
    }
}