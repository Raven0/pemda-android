package com.vndr.smartrenja.ui.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.vndr.smartrenja.databinding.ActivityGalleryBinding
import com.vndr.smartrenja.domain.model.Gallery
import com.vndr.smartrenja.ui.adapters.GalleryAdapter
import com.vndr.smartrenja.ui.fragments.PopupFragment
import com.vndr.smartrenja.util.fromJsonToken


class GalleryActivity : AppCompatActivity() {
    private val binding by lazy { ActivityGalleryBinding.inflate(layoutInflater) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        val staggeredGridLayoutManager = StaggeredGridLayoutManager(2, LinearLayoutManager.VERTICAL)
        binding.recyclerView.layoutManager = staggeredGridLayoutManager

        val gallery: List<Gallery> = fromJsonToken(intent.getStringExtra("gallery"))
        val galleryAdapter = GalleryAdapter(gallery)
        galleryAdapter.onItemClick = {
            PopupFragment.show(it, supportFragmentManager)
        }
        binding.recyclerView.adapter = galleryAdapter

        binding.imageViewBack.setOnClickListener {
            onBackPressed()
        }
    }
}