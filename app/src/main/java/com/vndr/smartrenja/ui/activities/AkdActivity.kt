package com.vndr.smartrenja.ui.activities

import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import com.rajat.pdfviewer.PdfViewerActivity
import com.vndr.smartrenja.R
import com.vndr.smartrenja.databinding.ActivityAkdBinding
import com.vndr.smartrenja.domain.model.ProgramKerja
import com.vndr.smartrenja.domain.source.model.User
import com.vndr.smartrenja.domain.source.model.state.Resource
import com.vndr.smartrenja.domain.util.Constants
import com.vndr.smartrenja.extention.makeGone
import com.vndr.smartrenja.extention.makeVisible
import com.vndr.smartrenja.extention.toast
import com.vndr.smartrenja.ui.adapters.MemberAdapter
import com.vndr.smartrenja.ui.adapters.ProgjaAdapter
import com.vndr.smartrenja.util.setOnSafeClickListener
import com.vndr.smartrenja.viewmodel.AkdViewModel
import com.vndr.smartrenja.viewmodel.MainViewModel

class AkdActivity : AppCompatActivity() {
    private val mainViewModel: MainViewModel by viewModels()
    private val akdViewModel: AkdViewModel by viewModels()
    private val binding by lazy { ActivityAkdBinding.inflate(layoutInflater) }

    private lateinit var progjaAdapter: ProgjaAdapter
    private val memberAdapter by lazy { MemberAdapter() }
    private var akdId: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        binding.textViewAkd.text = intent.getStringExtra(Constants.INTENT_AKD_NAME)
        akdId = intent.getIntExtra(Constants.INTENT_AKD_ID, 0)

        mainViewModel.userLiveData.observe(this, {
            when (it) {
                is Resource.Loading -> {
                }
                is Resource.Success -> it.data?.let { user ->
                    bindUi(user)
                    binding.cardViewRenja.setOnSafeClickListener {
                        startActivity(
                            PdfViewerActivity.buildIntent(
                                this,
                                user.fileRenjaDprd,
                                false,
                                getString(R.string.rencana_kerja_dprd),
                                "",
                                enableDownload = true
                            )
                        )
                    }
                }
                is Resource.Error -> {
                    toast { it.getStringMessage() }
                }
            }
        })

        akdViewModel.anggotaLiveData.observe(this, {
            when (it) {
                is Resource.Loading -> {
                    binding.progressBar.makeVisible()
                }
                is Resource.Success -> it.data?.let { anggota ->
                    binding.progressBar.makeGone()
                    memberAdapter.addAll(anggota)
                }
                is Resource.Error -> {
                    binding.progressBar.makeGone()
                    binding.textViewStatus.text = getString(R.string.label_not_found)
                }
            }
        })

        mainViewModel.fetchUser()

        binding.imageViewBack.setOnClickListener {
            onBackPressed()
        }

        binding.refreshLayout.setOnRefreshListener {
            mainViewModel.loadBanners()
            mainViewModel.loadUpcomingSchedules()
        }
    }

    private fun bindUi(user: User) {
        binding.refreshLayout.isRefreshing = false

        user.akd?.firstOrNull { it.akdId == akdId }.apply {
            progjaAdapter = if (this?.programKerja?.isNotEmpty() == true)
                this.programKerja?.let { ProgjaAdapter(it) }!!
            else ProgjaAdapter(
                listOf(
                    ProgramKerja(
                        0, 0, getString(R.string.label_not_found), 0, 0, 0
                    )
                )
            )
        }

        binding.recyclerviewProgja.adapter = progjaAdapter
        binding.recyclerviewMember.adapter = memberAdapter

        akdViewModel.loadAnggota(akdId)
    }
}