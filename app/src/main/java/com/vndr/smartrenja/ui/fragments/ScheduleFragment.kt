package com.vndr.smartrenja.ui.fragments

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.view.children
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.kizitonwose.calendarview.model.CalendarDay
import com.kizitonwose.calendarview.model.CalendarMonth
import com.kizitonwose.calendarview.model.DayOwner
import com.kizitonwose.calendarview.ui.DayBinder
import com.kizitonwose.calendarview.ui.MonthHeaderFooterBinder
import com.kizitonwose.calendarview.ui.ViewContainer
import com.vndr.smartrenja.R
import com.vndr.smartrenja.databinding.CalendarDayBinding
import com.vndr.smartrenja.databinding.CalendarHeaderBinding
import com.vndr.smartrenja.databinding.FragmentScheduleBinding
import com.vndr.smartrenja.domain.model.Schedule
import com.vndr.smartrenja.domain.source.model.state.Resource
import com.vndr.smartrenja.extention.*
import com.vndr.smartrenja.extention.setTextColorRes
import com.vndr.smartrenja.ui.adapters.ScheduleAdapter
import com.vndr.smartrenja.viewmodel.MainViewModel
import java.time.DayOfWeek
import java.time.LocalDate
import java.time.YearMonth
import java.time.format.DateTimeFormatter

class ScheduleFragment : Fragment() {
    private lateinit var binding: FragmentScheduleBinding
    private val mainViewModel: MainViewModel by activityViewModels()

    private val scheduleAdapter by lazy { ScheduleAdapter() }

    private var selectedDate: LocalDate? = null
    private val today = LocalDate.now()
    private val monthTitleFormatter = DateTimeFormatter.ofPattern("MMMM")
    private var events = mutableListOf<Schedule>().groupBy { it.date }

    private lateinit var daysOfWeek: Array<DayOfWeek>
    private lateinit var currentMonth: YearMonth

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View {
        binding = FragmentScheduleBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        daysOfWeek = daysOfWeekFromLocale()
        currentMonth = YearMonth.now()

        mainViewModel.scheduleListLiveData.observe(viewLifecycleOwner, { resource ->
            when (resource) {
                Resource.Loading -> {
                }
                is Resource.Success -> {
                    resource.data?.let { bindSchedule(it) }
                }
                is Resource.Error.ApiError -> {
                    toast { resource.message }
                }
                is Resource.Error.Thrown -> {
                    toast { resource.exception.message.toString() }
                }
            }
        })

        binding.recyclerviewSchedule.apply {
            layoutManager = LinearLayoutManager(
                requireContext(),
                RecyclerView.VERTICAL, false
            )
            adapter = scheduleAdapter
            addItemDecoration(DividerItemDecoration(requireContext(), RecyclerView.VERTICAL))
        }

        binding.calendarView.apply {
            setup(
                currentMonth.minusMonths(12),
                currentMonth.plusMonths(12), daysOfWeek.first()
            )
            scrollToMonth(currentMonth)
        }

        bindCalendar()
    }

    private fun bindSchedule(schedule: List<Schedule>) {
        val list = mutableListOf<Schedule>()
        for (i in schedule) {
            val formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd")
            list.add(
                Schedule(
                    i.id,
                    i.kegiatanKerja,
                    i.programKerja,
                    i.tanggal,
                    i.temaRapat,
                    i.temaKunker,
                    i.dasarReses,
                    i.rapatId,
                    i.kunkerId,
                    i.resesId,
                    LocalDate.parse(i.tanggal, formatter)
                )
            )
        }
        events = list.groupBy { it.date }
        selectDate(today)
        bindCalendar()
    }

    @SuppressLint("SetTextI18n")
    private fun bindCalendar() {
        class DayViewContainer(view: View) : ViewContainer(view) {
            lateinit var day: CalendarDay // Will be set when this container is bound.
            val binding = CalendarDayBinding.bind(view)

            init {
                view.setOnClickListener {
                    if (day.owner == DayOwner.THIS_MONTH) {
                        selectDate(day.date)
                    }
                }
            }
        }

        binding.calendarView.dayBinder = object : DayBinder<DayViewContainer> {
            override fun create(view: View) = DayViewContainer(view)
            override fun bind(container: DayViewContainer, day: CalendarDay) {
                container.day = day
                val textView = container.binding.calendarDayText
                val dotView = container.binding.calendarDayBackground

                textView.text = day.date.dayOfMonth.toString()

                if (day.owner == DayOwner.THIS_MONTH) {
                    textView.makeVisible()
                    when (day.date) {
                        today -> {
                            textView.setTextColorRes(R.color.white)
                            textView.setBackgroundResource(R.drawable.bg_today)
                            dotView.makeInVisible()
                        }
                        selectedDate -> {
                            textView.setTextColorRes(R.color.white)
                            textView.setBackgroundResource(R.drawable.bg_selected_date)
                            dotView.makeInVisible()
                        }
                        else -> {
                            textView.setTextColorRes(R.color.black)
                            textView.background = null
                            dotView.isVisible = events[day.date].orEmpty().isNotEmpty()
                        }
                    }
                } else {
                    textView.setTextColorRes(R.color.grey_500)
                    dotView.isVisible = events[day.date].orEmpty().isNotEmpty()
                }
            }
        }

        class MonthViewContainer(view: View) : ViewContainer(view) {
            val legendLayout = CalendarHeaderBinding.bind(view).legendLayout.root
        }

        binding.calendarView.monthHeaderBinder =
            object : MonthHeaderFooterBinder<MonthViewContainer> {
                override fun create(view: View) = MonthViewContainer(view)
                override fun bind(container: MonthViewContainer, month: CalendarMonth) {
                    if (container.legendLayout.tag == null) {
                        container.legendLayout.tag = month.yearMonth
                        container.legendLayout.children.map { it as TextView }
                            .forEachIndexed { index, tv ->
                                tv.text = daysOfWeek[index].name.first().toString()
                                tv.setTextColorRes(R.color.black)
                            }
                    }
                }
            }

        binding.calendarView.monthScrollListener = {
            binding.textViewSelectedMonth.text =
                "${monthTitleFormatter.format(it.yearMonth)} ${it.yearMonth.year}"
        }
    }

    private fun selectDate(date: LocalDate) {
        if (selectedDate != date) {
            val oldDate = selectedDate
            selectedDate = date
            oldDate?.let { binding.calendarView.notifyDateChanged(it) }
            binding.calendarView.notifyDateChanged(date)
            updateAdapterForDate(date)
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun updateAdapterForDate(date: LocalDate) {
        scheduleAdapter.apply {
            events.clear()
            events.addAll(this@ScheduleFragment.events[date].orEmpty())
            notifyDataSetChanged()
        }
    }
}