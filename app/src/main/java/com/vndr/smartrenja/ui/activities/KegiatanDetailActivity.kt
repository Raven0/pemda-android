package com.vndr.smartrenja.ui.activities

import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import com.rajat.pdfviewer.PdfViewerActivity
import com.vndr.smartrenja.R
import com.vndr.smartrenja.databinding.ActivityKegiatanDetailBinding
import com.vndr.smartrenja.domain.model.Akd
import com.vndr.smartrenja.domain.model.Kunker
import com.vndr.smartrenja.domain.model.Rapat
import com.vndr.smartrenja.domain.model.Reses
import com.vndr.smartrenja.domain.source.local.PrefManager
import com.vndr.smartrenja.domain.source.model.state.Resource
import com.vndr.smartrenja.domain.util.Constants
import com.vndr.smartrenja.domain.util.Constants.ACTION_APPROVE
import com.vndr.smartrenja.domain.util.Constants.ACTION_REJECT
import com.vndr.smartrenja.domain.util.Constants.ACTION_VIEW
import com.vndr.smartrenja.extention.activityInjection
import com.vndr.smartrenja.extention.makeGone
import com.vndr.smartrenja.extention.makeVisible
import com.vndr.smartrenja.extention.toast
import com.vndr.smartrenja.ui.adapters.GalleryOverviewAdapter
import com.vndr.smartrenja.ui.adapters.ParticipantAdapter
import com.vndr.smartrenja.ui.fragments.PopupFragment
import com.vndr.smartrenja.util.DocumentDialog
import com.vndr.smartrenja.util.TimeConverter
import com.vndr.smartrenja.util.fromJson
import com.vndr.smartrenja.util.toJson
import com.vndr.smartrenja.viewmodel.KegiatanViewModel
import javax.inject.Inject

class KegiatanDetailActivity : AppCompatActivity() {

    @Inject
    lateinit var prefManager: PrefManager

    private val kegiatanViewModel: KegiatanViewModel by viewModels()
    private val binding by lazy { ActivityKegiatanDetailBinding.inflate(layoutInflater) }

    private val memberAdapter by lazy { ParticipantAdapter() }
    private val galleryAdapter by lazy { GalleryOverviewAdapter() }

    private lateinit var documentDialog: DocumentDialog
    private lateinit var selectedRapat: Rapat
    private lateinit var selectedKunker: Kunker
    private lateinit var selectedReses: Reses

    private lateinit var akd: Akd
    private lateinit var type: String
    private lateinit var typeIntent: String
    private lateinit var action: String
    private var id: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        activityInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        typeIntent = intent.getStringExtra(Constants.INTENT_KEGIATAN_TYPE).toString()
        id = intent.getIntExtra(Constants.INTENT_KEGIATAN_ID, 0)
        documentDialog = DocumentDialog(this)
        akd = prefManager.getMenuAkd()?.fromJson<Akd>()!!

        action = intent.getStringExtra("action_request").toString()

        binding.imageViewBack.setOnClickListener {
            onBackPressed()
        }

        bind()

    }

    private fun bind() {
        when (typeIntent) {
            Constants.RAPAT -> {
                binding.textViewA.text = "Tema Rapat:"
                binding.textViewB.text = "Sub Tema Rapat:"
                binding.textViewMemberName.text = "Pimpinan Rapat:"
                binding.textViewD.text = "File Rapat:"
                binding.textViewE.text = "Galeri"
                binding.textViewG.text = "Lihat lebih lanjut"

                kegiatanViewModel.rapatDetailLiveData.observe(this, {
                    when (it) {
                        is Resource.Loading -> {
                        }
                        is Resource.Success -> it.data?.let { data ->
                            binding.refreshLayout.isRefreshing = false
                            selectedRapat = data
                            bindUi()
                        }
                        is Resource.Error -> {
                            binding.refreshLayout.isRefreshing = false
                            toast { it.getStringMessage() }
                        }
                    }
                })

                kegiatanViewModel.approveLiveData.observe(this, {
                    when (it) {
                        is Resource.Loading -> {
                        }
                        is Resource.Success -> {
                            kegiatanViewModel.loadRapatDetail(id)
                            bindUi()
                        }
                        is Resource.Error -> {
                            toast { it.getStringMessage() }
                        }
                    }
                })
                kegiatanViewModel.loadRapatDetail(id)

                binding.refreshLayout.setOnRefreshListener {
                    kegiatanViewModel.loadRapatDetail(id)
                }
            }
            Constants.KUNKER -> {
                binding.textViewA.text = "Tema Kegiatan:"
                binding.textViewB.text = "Gambaran Umum:"
                binding.textViewMemberName.text = "Pimpinan kunker:"
                binding.textViewD.text = "File Kunker:"
                binding.textViewE.text = "Galeri"
                binding.textViewG.text = "Lihat lebih lanjut"

                kegiatanViewModel.kunkerDetailLiveData.observe(this, {
                    when (it) {
                        is Resource.Loading -> {
                        }
                        is Resource.Success -> it.data?.let { data ->
                            binding.refreshLayout.isRefreshing = false
                            selectedKunker = data
                            bindUi()
                        }
                        is Resource.Error -> {
                            binding.refreshLayout.isRefreshing = false
                            toast { it.getStringMessage() }
                        }
                    }
                })

                kegiatanViewModel.approveLiveData.observe(this, {
                    when (it) {
                        is Resource.Loading -> {
                        }
                        is Resource.Success -> {
                            kegiatanViewModel.loadKunkerDetail(id)
                            bindUi()
                        }
                        is Resource.Error -> {
                            toast { it.getStringMessage() }
                        }
                    }
                })

                kegiatanViewModel.loadKunkerDetail(id)

                binding.refreshLayout.setOnRefreshListener {
                    kegiatanViewModel.loadKunkerDetail(id)
                }
            }
            Constants.RESES -> {
                binding.textViewA.text = "Dasar Reses:"
                binding.textViewB.text = "Pelaksana Reses:"
                binding.textViewD.text = "File Reses:"
                binding.textViewE.text = "Galeri"
                binding.textViewG.text = "Lihat lebih lanjut"

                kegiatanViewModel.resesDetailLiveData.observe(this, {
                    when (it) {
                        is Resource.Loading -> {
                        }
                        is Resource.Success -> it.data?.let { data ->
                            binding.refreshLayout.isRefreshing = false
                            selectedReses = data
                            bindUi()
                        }
                        is Resource.Error -> {
                            binding.refreshLayout.isRefreshing = false
                            toast { it.getStringMessage() }
                        }
                    }
                })

                kegiatanViewModel.approveLiveData.observe(this, {
                    when (it) {
                        is Resource.Loading -> {
                        }
                        is Resource.Success -> {
                            kegiatanViewModel.loadResesDetail(id)
                            bindUi()
                        }
                        is Resource.Error -> {
                            toast { it.getStringMessage() }
                        }
                    }
                })

                kegiatanViewModel.loadResesDetail(id)

                binding.refreshLayout.setOnRefreshListener {
                    kegiatanViewModel.loadResesDetail(id)
                }
            }
        }
    }

    private fun bindUi() {
        when (typeIntent) {
            Constants.RAPAT -> {
                selectedRapat.peserta?.let { memberAdapter.addAll(it) }
                selectedRapat.galleries?.let { gallery ->
                    if (gallery.isNotEmpty()) {
                        binding.textViewE.makeVisible()
                        galleryAdapter.addAll(gallery.take(5))
                        if (gallery.size > 5) binding.textViewG.makeVisible()

                        binding.textViewG.setOnClickListener {
                            startActivity(
                                Intent(this, GalleryActivity::class.java)
                                    .putExtra("gallery", gallery.toJson())
                            )
                        }
                    }
                }

                documentDialog.onDialogClick = {
                    when (it) {
                        ACTION_VIEW -> {
                            startActivity(
                                PdfViewerActivity.buildIntent(
                                    this,
                                    if (type == "materi") selectedRapat.files?.file_materi else selectedRapat.files?.hasil_rapat,
                                    false,
                                    if (type == "materi") "${getString(R.string.materi_rapat)} ${selectedRapat.temaRapat}" else "${
                                        getString(
                                            R.string.hasil_rapat
                                        )
                                    } ${selectedRapat.temaRapat}",
                                    "",
                                    enableDownload = true
                                )
                            )
                        }
                        ACTION_APPROVE -> {
                            if (type == "materi") {
                                kegiatanViewModel.approveRapat(id)
                            } else {
                                kegiatanViewModel.approveHasilRapat(id)
                            }
                        }
                        ACTION_REJECT -> {
                            documentDialog.showFeedbackDialog()
                        }
                    }
                }

                documentDialog.onFeedbackDialogClick = {
                    if (type == "materi") {
                        kegiatanViewModel.rejectRapat(id, it)
                    } else {
                        kegiatanViewModel.rejectHasilRapat(id, it)
                    }
                }


                val data = selectedRapat

                //BIND
                binding.textViewTitle.text = "Detail Rapat"
                binding.textViewAValue.text = data.temaRapat
                binding.textViewBValue.text = data.subTemaRapat
                binding.textViewMemberNameReal.text = data.pimpinan
                binding.textViewC.text = data.jenis
                binding.textViewMemberTanggal.text =
                    data.tanggal?.let { TimeConverter.convertDateMin(it) }

                if (data.files?.file_materi_global != null) {
                    binding.cardViewFileEmpty.makeGone()
                    binding.cardViewFileA.makeVisible()
                    binding.textViewATitle.text = getString(R.string.materi_rapat) + " Global"
                    binding.textViewAContent.makeGone()
                    binding.cardViewFileA.setOnClickListener {
                        startActivity(
                            PdfViewerActivity.buildIntent(
                                this,
                                data.files?.file_materi_global,
                                false,
                                "${getString(R.string.materi_rapat)} Global ${data.temaRapat}",
                                "",
                                enableDownload = true
                            )
                        )
                    }
                }

                if (data.files?.file_materi != null) {
                    binding.cardViewFileEmpty.makeGone()
                    binding.cardViewFileB.makeVisible()
                    binding.textViewBTitle.text = getString(R.string.materi_rapat)
                    binding.textViewBContent.text = "Status: ${data.statusMateriRapat}"
                    binding.cardViewFileB.setOnClickListener {
                        if (akd.canApprove ?: true == true) {
                            data.statusMateriRapat?.let {
                                documentDialog.showDialog(
                                    "Materi Rapat ${data.temaRapat}",
                                    it, data.catatanMateriRapat
                                )
                            }
                            selectedRapat = data
                            type = "materi"
                        } else {
                            startActivity(
                                PdfViewerActivity.buildIntent(
                                    this,
                                    data.files?.file_materi,
                                    false,
                                    "${getString(R.string.materi_rapat)} ${data.temaRapat}",
                                    "",
                                    enableDownload = true
                                )
                            )
                        }
                    }
                }

                if (data.files?.hasil_rapat != null) {
                    binding.cardViewFileEmpty.makeGone()
                    binding.cardViewFileC.makeVisible()
                    binding.textViewCTitle.text = getString(R.string.hasil_rapat)
                    binding.textViewCContent.text = "Status: ${data.statusHasilRapat}"
                    binding.cardViewFileC.setOnClickListener {
                        if (akd.canApprove ?: true == true) {
                            data.statusHasilRapat?.let { it1 ->
                                documentDialog.showDialog(
                                    "Hasil Rapat ${data.temaRapat}",
                                    it1, data.catatanHasilRapat
                                )
                            }
                            selectedRapat = data
                            type = "hasil"
                        } else {
                            startActivity(
                                PdfViewerActivity.buildIntent(
                                    this,
                                    data.files?.hasil_rapat,
                                    false,
                                    "${getString(R.string.hasil_rapat)} ${data.temaRapat}",
                                    "",
                                    enableDownload = true
                                )
                            )
                        }
                    }
                }

                when (action) {
                    "approve_materi" -> {
                        data.statusMateriRapat?.let {
                            documentDialog.showDialog(
                                "Materi Rapat ${data.temaRapat}",
                                it, data.catatanMateriRapat
                            )
                        }
                        selectedRapat = data
                        type = "materi"

                        action = ""
                    }

                    "approve_hasil" -> {
                        data.statusHasilRapat?.let {
                            documentDialog.showDialog(
                                "Hasil Rapat ${data.temaRapat}",
                                it, data.catatanHasilRapat
                            )
                        }
                        selectedRapat = data
                        type = "hasil"

                        action = ""
                    }
                }
            }
            Constants.KUNKER -> {
                selectedKunker.peserta?.let { memberAdapter.addAll(it) }
                selectedKunker.galleries?.let { gallery ->
                    if (gallery.isNotEmpty()) {
                        binding.textViewE.makeVisible()
                        galleryAdapter.addAll(gallery.take(5))
                        if (gallery.size > 5) binding.textViewG.makeVisible()

                        binding.textViewG.setOnClickListener {
                            startActivity(
                                Intent(this, GalleryActivity::class.java)
                                    .putExtra("gallery", gallery.toJson())
                            )
                        }
                    }
                }

                documentDialog.onDialogClick = {
                    when (it) {
                        ACTION_VIEW -> {
                            startActivity(
                                PdfViewerActivity.buildIntent(
                                    this,
                                    if (type == "materi") selectedKunker.files?.file_materi else selectedKunker.files?.file_laporan_kunker,
                                    false,
                                    if (type == "materi") "Materi Kunker ${selectedKunker.temaKegiatan}" else "Laporan Kunker ${selectedKunker.temaKegiatan}",
                                    "",
                                    enableDownload = true
                                )
                            )
                        }
                        ACTION_APPROVE -> {
                            if (type == "materi") {
                                selectedKunker.id?.let { it1 -> kegiatanViewModel.approveKunker(it1) }
                            } else {
                                selectedKunker.id?.let { it1 ->
                                    kegiatanViewModel.approveLaporanKunker(
                                        it1
                                    )
                                }
                            }
                        }
                        ACTION_REJECT -> {
                            documentDialog.showFeedbackDialog()
                        }
                    }
                }

                documentDialog.onFeedbackDialogClick = {
                    if (type == "materi") {
                        selectedKunker.id?.let { it1 -> kegiatanViewModel.rejectKunker(it1, it) }
                    } else {
                        selectedKunker.id?.let { it1 ->
                            kegiatanViewModel.rejectLaporanKunker(
                                it1,
                                it
                            )
                        }
                    }
                }


                val data = selectedKunker
                //BIND
                binding.textViewTitle.text = "Detail Kunker"
                binding.textViewAValue.text = data.temaKegiatan
                binding.textViewBValue.text = data.gambaranUmum
                binding.textViewMemberNameReal.text = data.pimpinanKunker
                binding.textViewC.text = data.lokasiTujuan
                binding.textViewMemberTanggal.text =
                    data.tanggal?.let { TimeConverter.convertDateMin(it) }

                if (data.files?.file_materi != null) {
                    binding.cardViewFileEmpty.makeGone()
                    binding.cardViewFileB.makeVisible()
                    binding.textViewBTitle.text = "Materi Kunker"
                    binding.textViewBContent.text = "Status: ${data.statusMateriKunker}"
                    binding.cardViewFileB.setOnClickListener {
                        if (akd.canApprove ?: true == true) {
                            data.statusMateriKunker?.let {
                                documentDialog.showDialog(
                                    "Materi Kunker ${data.temaKegiatan}",
                                    it, data.catatanMateriKunker
                                )
                            }
                            selectedKunker = data
                            type = "materi"
                        } else {
                            startActivity(
                                PdfViewerActivity.buildIntent(
                                    this,
                                    data.files?.file_materi,
                                    false,
                                    "Materi Kunker ${data.temaKegiatan}",
                                    "",
                                    enableDownload = true
                                )
                            )
                        }
                    }
                }

                if (data.files?.file_laporan_kunker != null) {
                    binding.cardViewFileEmpty.makeGone()
                    binding.cardViewFileC.makeVisible()
                    binding.textViewCTitle.text = "Laporan Kunker"
                    binding.textViewCContent.text = "Status: ${data.statusLaporanKunker}"
                    binding.cardViewFileC.setOnClickListener {
                        if (akd.canApprove ?: true == true) {
                            data.statusLaporanKunker?.let {
                                documentDialog.showDialog(
                                    "Laporan Kunker ${data.temaKegiatan}",
                                    it, data.catatanLaporanKunker
                                )
                            }
                            selectedKunker = data
                            type = "hasil"
                        } else {
                            startActivity(
                                PdfViewerActivity.buildIntent(
                                    this,
                                    data.files?.file_laporan_kunker,
                                    false,
                                    "Laporan Kunker ${data.temaKegiatan}",
                                    "",
                                    enableDownload = true
                                )
                            )
                        }
                    }
                }

                when (action) {
                    "approve_materi" -> {
                        data.statusMateriKunker?.let {
                            documentDialog.showDialog(
                                "Materi Kunker ${data.temaKegiatan}",
                                it, data.catatanMateriKunker
                            )
                        }
                        selectedKunker = data
                        type = "materi"

                        action = ""
                    }

                    "approve_hasil" -> {
                        data.statusLaporanKunker?.let {
                            documentDialog.showDialog(
                                "Laporan Kunker ${data.temaKegiatan}",
                                it, data.catatanLaporanKunker
                            )
                        }
                        selectedKunker = data
                        type = "hasil"

                        action = ""
                    }
                }
            }
            Constants.RESES -> {
                selectedReses.peserta?.let { memberAdapter.addAll(it) }
                selectedReses.galleries?.let {
                    if (it.isNotEmpty()) {
                        binding.textViewE.makeVisible()
                        galleryAdapter.addAll(it.take(5))
                        if (it.size > 5) binding.textViewG.makeVisible()
                    }
                }

                documentDialog.onDialogClick = {
                    when (it) {
                        ACTION_VIEW -> {
                            startActivity(
                                PdfViewerActivity.buildIntent(
                                    this,
                                    if (type == "materi") selectedReses.files?.file_materi_reses else selectedReses.files?.file_laporan_reses,
                                    false,
                                    if (type == "materi") "Materi Reses ${selectedReses.masa_sidang}" else "Laporan Reses ${selectedReses.masa_sidang}",
                                    "",
                                    enableDownload = true
                                )
                            )
                        }
                        ACTION_APPROVE -> {
                            if (type == "materi") {
                                selectedReses.id?.let { it1 -> kegiatanViewModel.approveReses(it1) }
                            } else {
                                selectedReses.id?.let { it1 ->
                                    kegiatanViewModel.approveHasilReses(
                                        it1
                                    )
                                }
                            }
                        }
                        ACTION_REJECT -> {
                            documentDialog.showFeedbackDialog()
                        }
                    }
                }

                documentDialog.onFeedbackDialogClick = {
                    if (type == "materi") {
                        selectedReses.id?.let { it1 -> kegiatanViewModel.rejectReses(it1, it) }
                    } else {
                        selectedReses.id?.let { it1 -> kegiatanViewModel.rejectReses(it1, it) }
                    }
                }


                val data = selectedReses

                //BIND
                binding.textViewTitle.text = "Detail Reses"
                binding.textViewAValue.text = data.dasarReses
                binding.textViewBValue.text = data.pelaksanaReses
                binding.textViewC.text = "Dapil ${data.dapil}"
                binding.textViewMemberName.text = "Lokasi Reses: ${data.lokasi_reses}"
                binding.textViewMemberNameReal.text = "Masa Sidang: ${data.masa_sidang}"
                binding.textViewMemberTanggal.text =
                    "${data.tanggal_awal?.let { TimeConverter.convertDateMin(it) }} - ${
                        data.tanggal_akhir?.let {
                            TimeConverter.convertDateMin(
                                it
                            )
                        }
                    }"

                if (data.files?.file_materi_reses != null) {
                    binding.cardViewFileEmpty.makeGone()
                    binding.cardViewFileB.makeVisible()
                    binding.textViewBTitle.text = "Materi Reses"
                    binding.textViewBContent.text = "Status: ${data.statusMateriRapat}"
                    binding.cardViewFileB.setOnClickListener {
                        startActivity(
                            PdfViewerActivity.buildIntent(
                                this,
                                data.files?.file_materi_reses,
                                false,
                                "Materi Reses ${data.masa_sidang}",
                                "",
                                enableDownload = true
                            )
                        )
                    }
                }

                if (data.files?.file_laporan_reses != null) {
                    binding.cardViewFileEmpty.makeGone()
                    binding.cardViewFileC.makeVisible()
                    binding.textViewCTitle.text = "Laporan Reses"
                    binding.textViewCContent.text = "Status ${data.statusLaporanRapat}"
                    binding.cardViewFileC.setOnClickListener {
                        startActivity(
                            PdfViewerActivity.buildIntent(
                                this,
                                data.files?.file_laporan_reses,
                                false,
                                "Laporan Reses ${data.masa_sidang}",
                                "",
                                enableDownload = true
                            )
                        )
                    }
                }
            }
        }
        binding.recyclerviewMember.adapter = memberAdapter
        binding.recyclerviewGaleri.adapter = galleryAdapter

        galleryAdapter.onItemClick = {
            PopupFragment.show(it, supportFragmentManager)
        }
    }
}