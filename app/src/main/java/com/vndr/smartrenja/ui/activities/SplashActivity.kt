package com.vndr.smartrenja.ui.activities

import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintSet
import androidx.transition.TransitionManager
import com.vndr.smartrenja.R
import com.vndr.smartrenja.databinding.ActivitySplashBinding
import com.vndr.smartrenja.domain.source.local.PrefManager
import com.vndr.smartrenja.domain.source.model.state.Resource
import com.vndr.smartrenja.extention.activityInjection
import com.vndr.smartrenja.util.setOnSafeClickListener
import com.vndr.smartrenja.util.startAtTop
import com.vndr.smartrenja.viewmodel.MainViewModel
import javax.inject.Inject

class SplashActivity : AppCompatActivity() {

    @Inject
    lateinit var prefManager: PrefManager

    private val viewModel: MainViewModel by viewModels()
    private lateinit var binding: ActivitySplashBinding

    private val loadingSet by lazy {
        ConstraintSet().apply {
            clone(binding.root)
            setVisibility(binding.loadingGroup.id, ConstraintSet.VISIBLE)
            setVisibility(binding.errorGroup.id, ConstraintSet.GONE)
        }
    }

    private val errorSet by lazy {
        ConstraintSet().apply {
            clone(binding.root)
            setVisibility(binding.loadingGroup.id, ConstraintSet.GONE)
            setVisibility(binding.errorGroup.id, ConstraintSet.VISIBLE)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        activityInjection.inject(this)
        super.onCreate(savedInstanceState)
        binding = ActivitySplashBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.errorBtn.setOnSafeClickListener {
            viewModel.getUser()
        }

        viewModel.userLiveData.observe(this, {
            TransitionManager.beginDelayedTransition(binding.root)
            when (it) {
                is Resource.Loading -> {
                    loadingSet.applyTo(binding.root)
                    binding.loadingTv.text = getString(R.string.label_loading)
                    binding.loadingIndicator.isIndeterminate = true
                }
                is Resource.Success -> if (it.data != null) {
                    if (prefManager.getLogin()) startActivity(
                        Intent(
                            this,
                            MainActivity::class.java
                        ).startAtTop()
                    ) else startActivity(
                        Intent(this, LoginActivity::class.java).startAtTop()
                    )
                }
                is Resource.Error -> {
                    //binding.errorTv.text = it.getUiMessage().toString()
                    //errorSet.applyTo(binding.root)
                    startActivity(
                        Intent(this, LoginActivity::class.java).startAtTop()
                    )
                }
            }
        })

        viewModel.getUser()
    }
}