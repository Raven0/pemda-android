@file:Suppress("DEPRECATION")

package com.vndr.smartrenja.ui.adapters

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.vndr.smartrenja.domain.util.Constants
import com.vndr.smartrenja.domain.util.Constants.KUNKER
import com.vndr.smartrenja.domain.util.Constants.PELAKSANAAN
import com.vndr.smartrenja.domain.util.Constants.PELAPORAN
import com.vndr.smartrenja.domain.util.Constants.PERENCANAAN
import com.vndr.smartrenja.domain.util.Constants.RAPAT
import com.vndr.smartrenja.ui.fragments.DataFragment

class KegiatanPagerAdapter(fm: FragmentManager) :
    FragmentPagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {
    private val pages = listOf(
        DataFragment.newInstance(PELAKSANAAN, Constants.TYPE_RAPAT, 0, false),
        DataFragment.newInstance(PELAKSANAAN, Constants.TYPE_KUNKER, 0, false)
    )

    override fun getItem(position: Int): Fragment {
        return pages[position]
    }

    override fun getCount(): Int {
        return pages.size
    }

    override fun getPageTitle(position: Int): CharSequence {
        return when (position) {
            0 -> RAPAT
            else -> KUNKER
        }
    }
}