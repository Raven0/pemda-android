package com.vndr.smartrenja.ui.fragments

import android.content.Intent
import android.os.Bundle
import android.transition.AutoTransition
import android.transition.TransitionManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.vndr.smartrenja.R
import com.vndr.smartrenja.databinding.FragmentCouncilBinding
import com.vndr.smartrenja.domain.model.Faqs
import com.vndr.smartrenja.domain.source.model.state.Resource
import com.vndr.smartrenja.ui.activities.LoginActivity
import com.vndr.smartrenja.ui.adapters.AkdMenuAdapter
import com.vndr.smartrenja.viewmodel.MainViewModel

class CouncilFragment : Fragment() {
    private lateinit var binding: FragmentCouncilBinding
    private val mainViewModel: MainViewModel by activityViewModels()

    private val akdMenuAdapter by lazy { AkdMenuAdapter() }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View {
        binding = FragmentCouncilBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.recyclerviewGridAkd.adapter = akdMenuAdapter

        mainViewModel.userLiveData.observe(viewLifecycleOwner, { resource ->
            when (resource) {
                Resource.Loading -> {
                }
                is Resource.Success -> resource.data?.let {
                    it.akd?.let { it1 -> akdMenuAdapter.addAll(it1) }
                }
                is Resource.Error -> {
                }
            }
        })

        mainViewModel.faqsListLiveData.observe(viewLifecycleOwner, { resource ->
            when (resource) {
                Resource.Loading -> {
                }
                is Resource.Success -> {
                    binding.textViewFaqContent.text = resource.data?.let { bindFaqs(it) }
                }
                is Resource.Error -> {
                }
            }
        })

        mainViewModel.termsListLiveData.observe(viewLifecycleOwner, { resource ->
            when (resource) {
                Resource.Loading -> {
                }
                is Resource.Success -> {
                    binding.textViewTermsContent.text = resource.data?.let { bindFaqs(it) }
                }
                is Resource.Error -> {
                }
            }
        })

        mainViewModel.logoutLiveData.observe(viewLifecycleOwner, {
            when (it) {
                Resource.Loading -> {
                }
                is Resource.Success -> {
                    if (it.data == true) context?.startActivity(
                        Intent(
                            context,
                            LoginActivity::class.java
                        )
                    )
                }
                is Resource.Error -> {
                }
            }
        })

        binding.textViewFaq.setOnClickListener {
            if (binding.textViewFaqContent.isVisible) {
                TransitionManager.beginDelayedTransition(binding.cardViewFaqs, AutoTransition())
                binding.textViewFaqContent.visibility = View.GONE

                binding.imageViewExpandFaq.setImageResource(R.drawable.ic_expand)
            } else {
                TransitionManager.beginDelayedTransition(binding.cardViewFaqs, AutoTransition())
                binding.textViewFaqContent.visibility = View.VISIBLE

                binding.imageViewExpandFaq.setImageResource(R.drawable.ic_minimize)
            }
        }

        binding.textViewTerms.setOnClickListener {
            if (binding.textViewTermsContent.isVisible) {
                TransitionManager.beginDelayedTransition(binding.cardViewFaqs, AutoTransition())
                binding.textViewTermsContent.visibility = View.GONE

                binding.imageViewExpandTerms.setImageResource(R.drawable.ic_expand)
            } else {
                TransitionManager.beginDelayedTransition(binding.cardViewFaqs, AutoTransition())
                binding.textViewTermsContent.visibility = View.VISIBLE

                binding.imageViewExpandTerms.setImageResource(R.drawable.ic_minimize)
            }
        }

        binding.cardViewLogout.setOnClickListener {
            mainViewModel.logout()
        }
    }

    private fun bindFaqs(faqs: List<Faqs>): String {
        var i = 0
        val s = StringBuilder()
        for (faq in faqs) {
            s.append("${faq.description}")
            i++
            if (faqs.size != i) s.append("\n")
        }
        return s.toString()
    }
}