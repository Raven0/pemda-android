package com.vndr.smartrenja.ui.adapters

import android.annotation.SuppressLint
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import coil.load
import coil.size.Scale
import coil.transform.RoundedCornersTransformation
import com.vndr.smartrenja.databinding.ItemMemberBinding
import com.vndr.smartrenja.domain.model.Anggota
import com.vndr.smartrenja.extention.layoutInflater
import java.util.*

class MemberAdapter : RecyclerView.Adapter<MemberAdapter.ViewHolder>() {
    private var data: MutableList<Anggota> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            ItemMemberBinding.inflate(parent.context.layoutInflater, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return data.size
    }

    @SuppressLint("NotifyDataSetChanged")
    fun addAll(list: List<Anggota>) {
        data.clear()
        for (i in list) {
            data.add(i)
        }
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        viewHolder.bind(data[position])
    }

    inner class ViewHolder(private val binding: ItemMemberBinding) :
        RecyclerView.ViewHolder(binding.root) {

        @SuppressLint("SetTextI18n")
        fun bind(data: Anggota) {
            binding.textViewMemberName.text = data.nama
            binding.textViewMemberPosition.text = data.jabatan
            binding.imageViewPicture.load(data.photo) {
                scale(Scale.FILL)
                transformations(RoundedCornersTransformation(12f))
            }
        }
    }
}