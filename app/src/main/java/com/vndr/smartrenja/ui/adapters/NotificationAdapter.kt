package com.vndr.smartrenja.ui.adapters

import android.annotation.SuppressLint
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import coil.load
import coil.size.Scale
import coil.transform.RoundedCornersTransformation
import com.vndr.smartrenja.databinding.ItemNotificationBinding
import com.vndr.smartrenja.domain.model.Notification
import com.vndr.smartrenja.domain.source.model.User
import com.vndr.smartrenja.extention.layoutInflater
import java.util.ArrayList

class NotificationAdapter(val user: User) : RecyclerView.Adapter<NotificationAdapter.ViewHolder>() {
    var onItemClick: ((Notification) -> Unit)? = null
    private var data: MutableList<Notification> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            ItemNotificationBinding.inflate(parent.context.layoutInflater, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return data.size
    }

    @SuppressLint("NotifyDataSetChanged")
    fun addAll(list: List<Notification>) {
        data.clear()
        for (i in list) {
            data.add(i)
        }
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        viewHolder.bind(data[position])
    }

    inner class ViewHolder(private val binding: ItemNotificationBinding) :
        RecyclerView.ViewHolder(binding.root) {

        @SuppressLint("SetTextI18n")
        fun bind(data: Notification) {
            data.content?.let {
                binding.textViewNotifTitle.text = it.title
                binding.textViewNotifContent.text = "[ ${it.akdName} ${it.kegiatan} ] ${it.body}"
                binding.textViewNotifDate.text = it.tanggal
            }

            binding.imageViewUserPicture.load(user.photo) {
                scale(Scale.FILL)
                transformations(RoundedCornersTransformation(12f))
            }

            itemView.setOnLongClickListener {
                onItemClick?.invoke(data)
                this@NotificationAdapter.data.removeAt(bindingAdapterPosition)
                notifyItemRemoved(bindingAdapterPosition)
                true
            }
        }
    }
}