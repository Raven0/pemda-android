package com.vndr.smartrenja.ui.fragments

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.rajat.pdfviewer.PdfViewerActivity
import com.vndr.smartrenja.R
import com.vndr.smartrenja.databinding.FragmentDataBinding
import com.vndr.smartrenja.domain.model.Renja
import com.vndr.smartrenja.domain.source.model.state.Resource
import com.vndr.smartrenja.domain.util.Constants.ACTION_APPROVE
import com.vndr.smartrenja.domain.util.Constants.ACTION_REJECT
import com.vndr.smartrenja.domain.util.Constants.ACTION_VIEW
import com.vndr.smartrenja.domain.util.Constants.PELAKSANAAN
import com.vndr.smartrenja.domain.util.Constants.PELAPORAN
import com.vndr.smartrenja.domain.util.Constants.PERENCANAAN
import com.vndr.smartrenja.domain.util.Constants.TYPE_ALL
import com.vndr.smartrenja.domain.util.Constants.TYPE_KUNKER
import com.vndr.smartrenja.domain.util.Constants.TYPE_RAPAT
import com.vndr.smartrenja.domain.util.Constants.TYPE_RESES
import com.vndr.smartrenja.extention.makeGone
import com.vndr.smartrenja.extention.makeVisible
import com.vndr.smartrenja.extention.toast
import com.vndr.smartrenja.ui.activities.ProgjaActivity
import com.vndr.smartrenja.ui.adapters.*
import com.vndr.smartrenja.util.DocumentDialog
import com.vndr.smartrenja.viewmodel.KegiatanViewModel
import com.vndr.smartrenja.viewmodel.ProgjaViewModel

@SuppressLint("NotifyDataSetChanged")
class DataFragment : Fragment() {
    private lateinit var binding: FragmentDataBinding
    private val progjaViewModel: ProgjaViewModel by activityViewModels()
    private val kegiatanViewModel: KegiatanViewModel by activityViewModels()

    private val scheduleAdapter by lazy { ScheduleAdapter() }
    private val rapatAdapter by lazy { RapatAdapter() }
    private val kunkerAdapter by lazy { KunkerAdapter() }
    private val resesAdapter by lazy { ResesAdapter() }
    private lateinit var renjaAdapter: RenjaAdapter
    private lateinit var documentDialog: DocumentDialog
    private lateinit var selectedRenja: Renja
    private lateinit var type: String
    private lateinit var scheduleType: String
    private var programKerjaId: Int = 0
    private var canApprove: Boolean = false
    private var renjaFetched: Boolean = false
    private var scheduleFetched: Boolean = false
    private var rapatFetched: Boolean = false
    private var kunkerFetched: Boolean = false
    private var resesFetched: Boolean = false

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View {
        binding = FragmentDataBinding.inflate(inflater, container, false)
        return binding.root
    }

    @SuppressLint("NotifyDataSetChanged")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        arguments?.getString(KEY_TYPE)?.let { type = it }
        arguments?.getString(KEY_SCHEDULE_TYPE)?.let { scheduleType = it }
        arguments?.getInt(KEY_PROGJA_ID)?.let { programKerjaId = it }
        arguments?.getBoolean(KEY_APPROVE)?.let { canApprove = it }
        documentDialog = DocumentDialog(requireContext())
        renjaAdapter = RenjaAdapter(type, programKerjaId)

        bindUi()
    }

    private fun bindUi() {
        when (scheduleType) {
            TYPE_ALL -> {
                progjaViewModel.renjaLiveData.observe(viewLifecycleOwner, {
                    when (it) {
                        is Resource.Loading -> {
                            binding.progressLayout.makeVisible()
                        }
                        is Resource.Success -> it.data?.let { renja ->
                            binding.progressLayout.makeGone()
                            renjaFetched = renja.isNotEmpty()
                            if (renjaFetched) {
                                binding.imageViewEmpty.makeGone()
                                renjaAdapter.apply {
                                    list.clear()
                                    list.addAll(renja)
                                    notifyDataSetChanged()
                                }
                            } else {
                                renjaAdapter.apply {
                                    list.clear()
                                    notifyDataSetChanged()
                                }
                                if (type == PERENCANAAN || type == PELAPORAN)
                                    binding.imageViewEmpty.makeVisible()
                            }
                            processQuery()
                        }
                        is Resource.Error -> {
                            toast { it.getStringMessage() }
                        }
                    }
                })

                progjaViewModel.scheduleLiveData.observe(viewLifecycleOwner, {
                    when (it) {
                        is Resource.Loading -> {
                            binding.progressLayout.makeVisible()
                        }
                        is Resource.Success -> it.data?.let { schedule ->
                            binding.progressLayout.makeGone()
                            scheduleFetched = schedule.isNotEmpty()
                            if (scheduleFetched) {
                                binding.imageViewEmpty.makeGone()
                                scheduleAdapter.apply {
                                    events.clear()
                                    events.addAll(schedule)
                                    notifyDataSetChanged()
                                }
                            } else {
                                scheduleAdapter.apply {
                                    events.clear()
                                    notifyDataSetChanged()
                                }
                                if (type == PELAKSANAAN)
                                    binding.imageViewEmpty.makeVisible()
                            }
                            processQuery()
                        }
                        is Resource.Error -> {
                            toast { it.getStringMessage() }
                        }
                    }
                })

                progjaViewModel.approveLiveData.observe(viewLifecycleOwner, {
                    if (it is Resource.Success) (activity as ProgjaActivity).getData()
                })
            }
            else -> {
                kegiatanViewModel.rapatLiveData.observe(viewLifecycleOwner, {
                    when (it) {
                        is Resource.Loading -> {
                            binding.progressLayout.makeVisible()
                        }
                        is Resource.Success -> it.data?.let { rapat ->
                            binding.progressLayout.makeGone()
                            rapatFetched = rapat.isNotEmpty()
                            if (rapatFetched) {
                                binding.imageViewEmpty.makeGone()
                                rapatAdapter.apply {
                                    data.clear()
                                    data.addAll(rapat)
                                    notifyDataSetChanged()
                                }
                            } else {
                                rapatAdapter.apply {
                                    data.clear()
                                    notifyDataSetChanged()
                                }
                                if (scheduleType == TYPE_RAPAT)
                                    binding.imageViewEmpty.makeVisible()
                            }
                            processQuery()
                        }
                        is Resource.Error -> {
                            toast { it.getStringMessage() }
                        }
                    }
                })

                kegiatanViewModel.kunkerLiveData.observe(viewLifecycleOwner, {
                    when (it) {
                        is Resource.Loading -> {
                            binding.progressLayout.makeVisible()
                        }
                        is Resource.Success -> it.data?.let { kunker ->
                            binding.progressLayout.makeGone()
                            kunkerFetched = kunker.isNotEmpty()
                            if (kunkerFetched) {
                                binding.imageViewEmpty.makeGone()
                                kunkerAdapter.apply {
                                    data.clear()
                                    data.addAll(kunker)
                                    notifyDataSetChanged()
                                }
                            } else {
                                kunkerAdapter.apply {
                                    data.clear()
                                    notifyDataSetChanged()
                                }
                                if (scheduleType == TYPE_KUNKER)
                                    binding.imageViewEmpty.makeVisible()
                            }
                            processQuery()
                        }
                        is Resource.Error -> {
                            toast { it.getStringMessage() }
                        }
                    }
                })

                kegiatanViewModel.resesLiveData.observe(viewLifecycleOwner, {
                    when (it) {
                        is Resource.Loading -> {
                            binding.progressLayout.makeVisible()
                        }
                        is Resource.Success -> it.data?.let { reses ->
                            binding.progressLayout.makeGone()
                            resesFetched = reses.isNotEmpty()
                            if (resesFetched) {
                                binding.imageViewEmpty.makeGone()
                                resesAdapter.apply {
                                    data.clear()
                                    data.addAll(reses)
                                    notifyDataSetChanged()
                                }
                            } else {
                                resesAdapter.apply {
                                    data.clear()
                                    notifyDataSetChanged()
                                }
                                if (scheduleType == TYPE_RESES)
                                    binding.imageViewEmpty.makeVisible()
                            }
                            processQuery()
                        }
                        is Resource.Error -> {
                            toast { it.getStringMessage() }
                        }
                    }
                })
            }
        }

        when (type) {
            PERENCANAAN, PELAPORAN -> {
                binding.recyclerviewData.adapter = renjaAdapter
            }
            PELAKSANAAN -> {
                when (scheduleType) {
                    TYPE_ALL -> binding.recyclerviewData.adapter = scheduleAdapter
                    TYPE_RAPAT -> binding.recyclerviewData.adapter = rapatAdapter
                    TYPE_KUNKER -> binding.recyclerviewData.adapter = kunkerAdapter
                    TYPE_RESES -> binding.recyclerviewData.adapter = resesAdapter
                }
            }
        }

        renjaAdapter.onItemClick = {
            if (canApprove) {
                it.statusLaporanProgja?.let { it1 ->
                    documentDialog.showDialog(
                        "Laporan Progja ${it.nama}",
                        it1, it.catatan
                    )
                }
                selectedRenja = it
            } else {
                requireContext().startActivity(
                    PdfViewerActivity.buildIntent(
                        requireContext(),
                        it.fileLaporanProgja,
                        false,
                        requireContext().getString(R.string.laporan_progja),
                        "",
                        enableDownload = true
                    )
                )
            }
        }

        documentDialog.onDialogClick = {
            when (it) {
                ACTION_VIEW -> {
                    requireContext().startActivity(
                        PdfViewerActivity.buildIntent(
                            requireContext(),
                            selectedRenja.fileLaporanProgja,
                            false,
                            requireContext().getString(R.string.laporan_progja),
                            "",
                            enableDownload = true
                        )
                    )
                }
                ACTION_APPROVE -> {
                    selectedRenja.id?.let { it1 -> progjaViewModel.approve(it1) }
                }
                ACTION_REJECT -> {
                    documentDialog.showFeedbackDialog()
                }
            }
        }

        documentDialog.onFeedbackDialogClick = {
            selectedRenja.id?.let { it1 -> progjaViewModel.reject(it1, it) }
        }
    }

    private fun processQuery() {
        progjaViewModel.searchLiveData.observe(
            viewLifecycleOwner,
            {
                if (renjaFetched) renjaAdapter.filter.filter(it)
                if (scheduleFetched) scheduleAdapter.filter.filter(it)
                if (rapatFetched) rapatAdapter.filter.filter(it)
                if (kunkerFetched) kunkerAdapter.filter.filter(it)
                if (resesFetched) resesAdapter.filter.filter(it)
            })
    }

    companion object {
        private const val KEY_TYPE = "KEY_TYPE"
        private const val KEY_SCHEDULE_TYPE = "KEY_SCHEDULE_TYPE"
        private const val KEY_PROGJA_ID = "KEY_PROGJA_ID"
        private const val KEY_APPROVE = "KEY_APPROVE"

        @JvmStatic
        fun newInstance(
            type: String,
            scheduleType: String,
            programKerjaId: Int,
            canApprove: Boolean
        ) =
            DataFragment().apply {
                arguments = Bundle().apply {
                    putString(KEY_TYPE, type)
                    putString(KEY_SCHEDULE_TYPE, scheduleType)
                    putInt(KEY_PROGJA_ID, programKerjaId)
                    putBoolean(KEY_APPROVE, canApprove)
                }
            }
    }
}