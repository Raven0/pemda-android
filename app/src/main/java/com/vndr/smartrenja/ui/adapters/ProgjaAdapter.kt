package com.vndr.smartrenja.ui.adapters

import android.annotation.SuppressLint
import android.content.Intent
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.vndr.smartrenja.R
import com.vndr.smartrenja.databinding.ItemProgjaBinding
import com.vndr.smartrenja.domain.model.ProgramKerja
import com.vndr.smartrenja.domain.util.Constants
import com.vndr.smartrenja.extention.layoutInflater
import com.vndr.smartrenja.ui.activities.ProgjaActivity

class ProgjaAdapter(val programKerja: List<ProgramKerja>) :
    RecyclerView.Adapter<ProgjaAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            ItemProgjaBinding.inflate(parent.context.layoutInflater, parent, false)
        )
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        viewHolder.bind(programKerja[position])

        if (position == programKerja.lastIndex) {
            val params = viewHolder.itemView.layoutParams as RecyclerView.LayoutParams
            params.marginEnd = 32
            viewHolder.itemView.layoutParams = params
        }
    }

    override fun getItemCount(): Int = programKerja.size

    @SuppressLint("SetTextI18n")
    inner class ViewHolder(private val binding: ItemProgjaBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(data: ProgramKerja) {
            binding.textViewProgjaTitle.text = data.namaProgram
            binding.textViewProgjaRenjaCount.text = data.totalRencanaKerja.toString()
            binding.textViewProgjaRapatCount.text = data.totalRapat.toString()
            binding.textViewProgjaKunkerCount.text = data.totalKunker.toString()
            data.programKerjaId?.let { bindByProgjaId(it) }

            binding.root.setOnClickListener {
                it.context.startActivity(
                    Intent(it.context, ProgjaActivity::class.java)
                        .putExtra(Constants.INTENT_PROGJA_NAME, data.namaProgram)
                        .putExtra(Constants.INTENT_PROGJA_ID, data.programKerjaId)
                )
            }
        }

        @SuppressLint("UseCompatLoadingForDrawables")
        private fun bindByProgjaId(programKerjaId: Int) {
            when (programKerjaId) {
                1 -> {
                    binding.imageViewProgja.setImageResource(R.drawable.img_pembentukan)
                    binding.constraintLayoutProgja.background =
                        binding.root.context.getDrawable(R.drawable.bg_gradient_red)
                }
                3 -> {
                    binding.imageViewProgja.setImageResource(R.drawable.img_pengawasan)
                    binding.constraintLayoutProgja.background =
                        binding.root.context.getDrawable(R.drawable.bg_gradient_green)
                }
                2 -> {
                    binding.imageViewProgja.setImageResource(R.drawable.img_penganggaran)
                    binding.constraintLayoutProgja.background =
                        binding.root.context.getDrawable(R.drawable.bg_gradient_orange)
                }
                else -> {
                    binding.imageViewProgja.setImageResource(R.drawable.img_peningkatan)
                    binding.constraintLayoutProgja.background =
                        binding.root.context.getDrawable(R.drawable.bg_gradient_blue)
                }
            }
        }
    }
}